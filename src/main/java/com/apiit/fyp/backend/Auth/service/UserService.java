package com.apiit.fyp.backend.Auth.service;



import com.apiit.fyp.backend.Auth.Model.UserDto;
import com.apiit.fyp.backend.Entity.User;

import java.util.List;

public interface UserService {

    User save(User user);

    List<User> findAll();

    void delete(int id);

    User findUser(String username);

    User findById(int id);

    UserDto update(UserDto userDto);
}
