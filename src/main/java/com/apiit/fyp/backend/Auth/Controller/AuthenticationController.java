package com.apiit.fyp.backend.Auth.Controller;


import com.apiit.fyp.backend.Auth.Config.JwtTokenUtil;
import com.apiit.fyp.backend.Auth.Model.AuthToken;
import com.apiit.fyp.backend.Auth.Model.LoginUser;
import com.apiit.fyp.backend.Auth.service.UserService;
import com.apiit.fyp.backend.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/token")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/generate-token", method = RequestMethod.POST)
    public AuthToken generateToken(@RequestBody LoginUser loginUser) throws AuthenticationException {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUsername(), loginUser.getPassword()));
        final User user = userService.findUser(loginUser.getUsername());
        final String token = jwtTokenUtil.generateToken(user);
        return new AuthToken(token, user.getUsername(), user.getUserId());
    }

//    @RequestMapping(value = "/logout", method = RequestMethod.POST)
//    public ApiResponse<Void> logout() throws AuthenticationException {
//        return new ApiResponse<>(200, "success",null);
//    }

}
