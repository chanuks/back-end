package com.apiit.fyp.backend.Auth.Model;

public class AuthToken {

    private String token;
    private String username;
    private int userId;

    public AuthToken(String token, String username, int userId) {
        this.token = token;
        this.username = username;
        this.userId = userId;
    }

    public AuthToken() {

    }

    public AuthToken(String token, String username) {
        this.token = token;
        this.username = username;
    }

    public AuthToken(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
