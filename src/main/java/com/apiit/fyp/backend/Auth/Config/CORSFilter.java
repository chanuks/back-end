package com.apiit.fyp.backend.Auth.Config;


import com.apiit.fyp.backend.Entity.ServingCalculation;
import com.apiit.fyp.backend.Entity.User;
import com.apiit.fyp.backend.Entity.UserInfo;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class CORSFilter implements Filter {

    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        System.out.println("Filtering on...........................................................");
        HttpServletResponse response = (HttpServletResponse) res;
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Authorization, Origin, Accept, Access-Control-Request-Method, Access-Control-Request-Headers");

        chain.doFilter(req, res);
    }

    public void init(FilterConfig filterConfig) {
    }

    public void destroy() {
    }

    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {
        return RepositoryRestConfigurer.withConfig(config -> {
            config.exposeIdsFor(User.class);
        });
    }

//	@Configuration
//	private class MyCoolConfiguration extends RepositoryRestMvcConfiguration {
//
//        public MyCoolConfiguration(ApplicationContext context, ObjectFactory<ConversionService> conversionService) {
//            super(context, conversionService);
//        }
//
//
//		protected void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
//			config.exposeIdsFor(UserInfo.class,User.class, ServingCalculation.class);
//		}
//	}

}