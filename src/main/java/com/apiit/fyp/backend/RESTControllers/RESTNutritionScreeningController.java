package com.apiit.fyp.backend.RESTControllers;


import com.apiit.fyp.backend.DTO.NutritionalScreeningDTO;
import com.apiit.fyp.backend.DTO.UserDTO;
import com.apiit.fyp.backend.Entity.NutritionalScreening;
import com.apiit.fyp.backend.Service.NutritionalScreeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class RESTNutritionScreeningController {

    @Autowired
    public NutritionalScreeningService nutritionalScreeningService;

    @PostMapping("/nutritionalScreening/save")
    public NutritionalScreening saveNutritionalScreening(@RequestBody NutritionalScreening nutritionalScreening){
        return nutritionalScreeningService.save(nutritionalScreening);
    }

    @PostMapping("/nutritionalScreening/getAll")
    public List<NutritionalScreeningDTO> getAllNutritionalScreening(@RequestBody UserDTO dto) {
        return nutritionalScreeningService.getNutritionalScreenings(dto.getUserId());
    }
}
