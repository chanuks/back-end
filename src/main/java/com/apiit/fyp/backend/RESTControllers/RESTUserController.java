package com.apiit.fyp.backend.RESTControllers;

import com.apiit.fyp.backend.Auth.Controller.AuthenticationController;
import com.apiit.fyp.backend.Auth.Model.AuthToken;
import com.apiit.fyp.backend.Auth.Model.LoginUser;
import com.apiit.fyp.backend.Auth.service.UserService;
import com.apiit.fyp.backend.Entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController

public class RESTUserController {

    @Autowired
    private UserService userServices;

    @Autowired
    private AuthenticationController authenticationController;

    @PostMapping("/restUsers/saveUser")
    public AuthToken saveUser(@RequestBody User user) {

        userServices.save(user);

        LoginUser loginUser = new LoginUser();

        loginUser.setUsername(user.getUsername());
        loginUser.setPassword(user.getPassword());

        AuthToken authToken = authenticationController.generateToken(loginUser);

        return authToken;


    }
}