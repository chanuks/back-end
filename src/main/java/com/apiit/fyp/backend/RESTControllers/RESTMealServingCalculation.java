package com.apiit.fyp.backend.RESTControllers;

import com.apiit.fyp.backend.DTO.MealDTO;
import com.apiit.fyp.backend.DTO.MealServingsDTO;
import com.apiit.fyp.backend.DTO.ServingCalculationDTO;
import com.apiit.fyp.backend.DTO.UserInfoDTO;
import com.apiit.fyp.backend.Entity.FoodExchange;
import com.apiit.fyp.backend.Entity.MealServings;
import com.apiit.fyp.backend.Entity.User;
import com.apiit.fyp.backend.Repository.UserDAO;
import com.apiit.fyp.backend.Service.MealGenerationService;
import com.apiit.fyp.backend.Service.MealServingCalculationService;
import com.apiit.fyp.backend.Service.ServingCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class RESTMealServingCalculation {

    @Autowired
    public ServingCalculationService servingCalculationService;


    @Autowired
    public MealServingCalculationService mealServingCalculationService;


    @Autowired
    public MealGenerationService mealGenerationService;

    @Autowired
    public UserDAO userDAO;

    @PostMapping("/getMealServings")
    public MealServingsDTO getMealServings(@RequestBody ServingCalculationDTO servingCalculationDTO) {
        //servingCalculationService.saveUserInfo(userInfo);

        return mealServingCalculationService.calculateMealServings(servingCalculationDTO);
        //return servingCalculationService.saveServingCalculation(servingCalculationService.calculateDailyServings(userInfo));

    }

    @GetMapping("/getFood")
    public List<FoodExchange> getFood(){

        String group = "Cereals";

        return mealGenerationService.getFoodByGroup(group);

    }

    @PostMapping("/getGeneratedFood")
    public MealDTO getGenerated(@RequestBody MealServingsDTO mealServings){
        return mealGenerationService.generateMeal(mealServings);
    }

    @PostMapping("/getMeal")
    public MealDTO getMeal(@RequestBody UserInfoDTO userInfoDTO){

        int userId = userInfoDTO.getUserId();
        User user = userDAO.getUserByUserId(userId);





        ServingCalculationDTO servingCalculation = servingCalculationService.calculateDailyServings(userInfoDTO);
        MealServingsDTO mealServings = mealServingCalculationService.calculateMealServings(servingCalculation);
        MealDTO meal = mealGenerationService.generateMeal(mealServings);
        return meal;
    }
}
