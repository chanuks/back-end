package com.apiit.fyp.backend.RESTControllers;

import com.apiit.fyp.backend.DTO.ServingCalculationDTO;
import com.apiit.fyp.backend.DTO.UserInfoDTO;
import com.apiit.fyp.backend.Service.ServingCalculationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class RESTServingCalculationController {

    @Autowired
    public ServingCalculationService servingCalculationService;

//
//    @PostMapping("/getDailyServings")
//    public ServingCalculation getDailyServings(@RequestBody NutritionalScreening nutritionalScreening) {
//            return  servingCalculationService.calculateDailyServings(nutritionalScreening);
//    }
    @PostMapping("/getDailyServings")
    public ServingCalculationDTO getDailyServings(@RequestBody UserInfoDTO userInfoDTO) {
          //servingCalculationService.saveUserInfo(userInfo);
          return  servingCalculationService.calculateDailyServings(userInfoDTO);
         //return servingCalculationService.saveServingCalculation(servingCalculationService.calculateDailyServings(userInfo));

    }
}
