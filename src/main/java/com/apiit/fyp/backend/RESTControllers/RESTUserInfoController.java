package com.apiit.fyp.backend.RESTControllers;

import com.apiit.fyp.backend.DTO.ServingCalculationDTO;
import com.apiit.fyp.backend.DTO.UserInfoDTO;
import com.apiit.fyp.backend.Entity.UserInfo;
import com.apiit.fyp.backend.Repository.UserDAO;
import com.apiit.fyp.backend.Repository.UserInfoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
public class RESTUserInfoController {

    @Autowired
    private UserInfoDAO userInfoDAO;

    @GetMapping("/getUserInfo/{id}")
    public UserInfoDTO getDailyServings(@PathVariable("id") int id) {
        //servingCalculationService.saveUserInfo(userInfo);
        UserInfo userInfo = userInfoDAO.getByUserUserId(id);

        UserInfoDTO userInformation = new UserInfoDTO();
        userInformation.setUserId(userInfo.getUser().getUserId());
        userInformation.setBreakfastFood(userInfo.getBreakfastFood());
        userInformation.setLunchFood(userInfo.getLunchFood());
        userInformation.setDinnerFood(userInfo.getDinnerFood());
        userInformation.setEatingPattern(userInfo.getEatingPattern());
        userInformation.setUserAllergies(userInfo.getUserAllergies());
        userInformation.setUserMedicalConditions(userInfo.getUserMedicalConditions());
        userInformation.setDer(userInfo.getDER());

        return userInformation;
        //return servingCalculationService.saveServingCalculation(servingCalculationService.calculateDailyServings(userInfo));

    }
}
