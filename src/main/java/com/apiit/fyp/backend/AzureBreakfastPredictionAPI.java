package com.apiit.fyp.backend;

import com.apiit.fyp.backend.DTO.predictionRequestDTO;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.StringReader;
import java.util.Arrays;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class AzureBreakfastPredictionAPI {


    @PostMapping("/api/predictBreakfast")
    public String getBreakfastPrediction(@RequestBody predictionRequestDTO requestDTO) {

        JSONObject obj = new JSONObject();

        JSONObject gparameters = new JSONObject();

        JSONObject Inputs = new JSONObject();

        JSONObject input1 = new JSONObject();

        JSONArray dataItems = new JSONArray();

        JSONArray item1 = new JSONArray();
        item1.add("Age");
        item1.add("Gender");
        item1.add("Location");
        item1.add("Household");
        item1.add("Breakfast");



        dataItems.add(item1);
        input1.put("ColumnNames", item1);



        JSONArray dataItems2 = new JSONArray();

        JSONArray item2 = new JSONArray();
        item2.add(requestDTO.getAge_Group());
        item2.add(requestDTO.getGender());
        item2.add(requestDTO.getLocation());
        item2.add(requestDTO.getHousehold());
        item2.add(requestDTO.getBreakfast());



        dataItems2.add(item2);
        input1.put("Values", dataItems2);

        Inputs.put("input1", input1);

        obj.put("Inputs", Inputs);

        obj.put("GlobalParameters", gparameters);


        RestTemplate restTemplate = new RestTemplate();


        String uri = "https://ussouthcentral.services.azureml.net/workspaces/c1c89f0ff09a45f4acf2633e59fa6ce5/services/9dd97b21a8e346eb95c16d4a2e87d400/execute?api-version=2.0&=true";
        String apikey = "9wNokxLTkLrz+IPiSjENWBWlvs3YUKkPjQwp9Pg6hW0gUyKYu3DdaNGz/8holMJLdiRJV67NZtEU5GzAMJeHQQ==";
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization","Bearer "+apikey);

        HttpEntity<JSONObject> entity = new HttpEntity<>(obj, headers);

        JSONObject response2 =  restTemplate.exchange(
                uri, HttpMethod.POST, entity, JSONObject.class).getBody();

        String a = response2.toString();
        JsonReader reader = Json.createReader(new StringReader(a));
        JsonObject jsonObject = reader.readObject();
        JsonObject results = jsonObject.getJsonObject("Results");
        JsonObject output1 = results.getJsonObject("output1");
        JsonObject value = output1.getJsonObject("value");
        JsonArray Values = value.getJsonArray("Values");

        JsonArray innerArray = Values.getJsonArray(0);
        String prediction = innerArray.getString(innerArray.size()-1);

        System.out.println(prediction);


        return prediction;
//        return  restTemplate.exchange(
//                uri, HttpMethod.POST, entity, JSONObject.class).getBody();











    }

}




