package com.apiit.fyp.backend;

import com.apiit.fyp.backend.DTO.predictionRequestDTO;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.json.Json;
import javax.json.JsonObject;
import java.util.Arrays;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
public class AzureAPI {



        @PostMapping("/api/predict")
        public String getPrediction(@RequestBody predictionRequestDTO requestDTO) {



            RestTemplate restTemplate = new RestTemplate();

            JsonObject request = Json.createObjectBuilder()
                    .add("Inputs", Json.createObjectBuilder()
                            .add("input1", Json.createObjectBuilder()
                                    .add("ColumnNames",Json.createArrayBuilder()
                                            .add("Age")
                                            .add("Gender")
                                            .add("Location")
                                            .add("Household")
                                            .add("Breakfast"))
                                    .add("Values",Json.createArrayBuilder()
                                            .add(Json.createArrayBuilder()
                                               .add(requestDTO.getGender())
                                               .add(requestDTO.getLocation())
                                               .add(requestDTO.getHousehold())
                                               .add(requestDTO.getBreakfast()))
                                            .add(Json.createArrayBuilder()
                                                    .add(requestDTO.getGender())
                                                    .add(requestDTO.getLocation())
                                                    .add(requestDTO.getHousehold())
                                                    .add(requestDTO.getBreakfast())))))
                    .add("GlobalParameters",Json.createObjectBuilder())
                    .build();

            System.out.println(request);
            JsonObject requestDTO1 = request;

            String uri = "https://ussouthcentral.services.azureml.net/workspaces/c1c89f0ff09a45f4acf2633e59fa6ce5/services/9dd97b21a8e346eb95c16d4a2e87d400/execute?api-version=2.0&details=true";
            String apikey = "9wNokxLTkLrz+IPiSjENWBWlvs3YUKkPjQwp9Pg6hW0gUyKYu3DdaNGz/8holMJLdiRJV67NZtEU5GzAMJeHQQ==";
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.set("Authorization","Bearer "+apikey);

            HttpEntity<JsonObject> entity = new HttpEntity<JsonObject>(request,headers);

            return restTemplate.exchange(
                    uri, HttpMethod.POST, entity, String.class).getBody();
        }
    }

