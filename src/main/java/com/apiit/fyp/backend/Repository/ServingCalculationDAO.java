package com.apiit.fyp.backend.Repository;

import com.apiit.fyp.backend.Entity.ServingCalculation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ServingCalculationDAO extends JpaRepository<ServingCalculation, Integer> {
}
