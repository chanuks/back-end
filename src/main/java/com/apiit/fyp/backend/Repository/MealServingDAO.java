package com.apiit.fyp.backend.Repository;

import com.apiit.fyp.backend.Entity.MealServings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface MealServingDAO extends JpaRepository<MealServings,Integer> {
}
