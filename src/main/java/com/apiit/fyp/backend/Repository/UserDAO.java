package com.apiit.fyp.backend.Repository;

import com.apiit.fyp.backend.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface UserDAO extends JpaRepository<User, Integer> {

    User findByUsername(String username);

    User getUserByUserId(int userId);
}
