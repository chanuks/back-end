package com.apiit.fyp.backend.Repository;


import com.apiit.fyp.backend.Entity.Meal;
import com.apiit.fyp.backend.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface MealDAO extends JpaRepository<Meal,Integer> {

    Meal getMealByUser( User user);
}
