package com.apiit.fyp.backend.Repository;

import com.apiit.fyp.backend.Entity.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
public interface UserInfoDAO extends JpaRepository<UserInfo, Integer> {

    UserInfo findByUserInfoId(int userInfoId);

    UserInfo findUserInfoByUser(int userId);
    UserInfo getByUserUserId(int userId);
}
