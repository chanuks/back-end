package com.apiit.fyp.backend.Repository;


import com.apiit.fyp.backend.Entity.FoodExchange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin
public interface FoodExchangeDAO extends JpaRepository<FoodExchange,Integer> {

    List<FoodExchange> findByFoodGroup(String foodGroup);

    List<FoodExchange> findByFoodType (String foodType);
}
