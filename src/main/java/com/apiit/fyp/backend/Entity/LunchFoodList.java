package com.apiit.fyp.backend.Entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "lunchFoodList")
public class LunchFoodList {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;



    @OneToMany(cascade = CascadeType.ALL)
    private List<Food> lunchFoods;
}
