package com.apiit.fyp.backend.Entity;


import lombok.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "foodExchange")
public class FoodExchange {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "foodExchangeId")
    private int id;

    @Column(name="tag")
    private String tag;


    @Column(name="foodGroup")
    private String foodGroup;

    @Column(name="food")
    private String food;

    @Column(name="amountPerServing")
    private double amountPerServing;

    @Column(name="unit")
    private String unit;

    @Nullable
    @Column(name="foodType")
    private String foodType;

    @Nullable
    @Column(name="allergyType")
    private String allergyType;
}
