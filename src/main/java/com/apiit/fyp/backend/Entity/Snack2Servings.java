package com.apiit.fyp.backend.Entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "snack2Servings")
public class Snack2Servings {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "Snack2ServingsId")
    private int id;

    @OneToOne(mappedBy = "snack2Servings")
    private MealServings mealServings;

    @Column(name="cereals")
    private double cereals;

    @Column(name="pulses")
    private double pulses;

    @Column(name="fruitsHighCHO")
    private double fruitsHighCHO;

    @Column(name = "fruitsMediumCHO")
    private double fruitsMediumCHO;

    @Column(name = "fruitsLowCHO")
    private double fruitsLowCHO;

    @Column(name = "vegetablesHighCHO")
    private double vegetablesHighCHO;

    @Column(name = "vegetablesMediumCHO")
    private double vegetablesMediumCHO;

    @Column(name = "vegetablesLowCHO")
    private double vegetablesLowCHO;

    @Column(name = "vegetablesGreenLeafy")
    private double vegetablesGreenLeafy;

    @Column(name = "milkPowdered")
    private double milkPowdered;

    @Column(name = "milkFresh")
    private double milkFresh;

    @Column(name = "meatLowFat")
    private double meatLowFat;

    @Column(name = "meatHighFat")
    private double meatHighFat;

    @Column(name = "egg")
    private double egg;

    @Column(name = "fatsOils")
    private double fatsOils;

    @Column(name = "coconut")
    private double coconut;
}
