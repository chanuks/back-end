package com.apiit.fyp.backend.Entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "meal")
public class Meal {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "mealId")
    private int mealId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "breakfastFoods_id", referencedColumnName = "id")
    private BreakfastFoodList breakfastFoods;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "snack1Foods_id", referencedColumnName = "id")
    private Snack1FoodList snack1Foods;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "lunchFoods_id", referencedColumnName = "id")
    private LunchFoodList lunchFoods;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "snack2Foods_id", referencedColumnName = "id")
    private Snack2FoodList snack2Foods;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "dinnerFoods_id", referencedColumnName = "id")
    private DinnerFoodList dinnerFoods;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_userId", referencedColumnName = "userId")
    private User user;
//    @Column(name = "userIdInteger")
//    private int userId;
}

