package com.apiit.fyp.backend.Entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "food")
public class Food {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "foodId")
    private int id;

    @Column(name="notes")
    private String notes;


    @Column(name="foodGroup")
    private String foodGroup;

    @Column(name="food")
    private String food;

    @Column(name="servingsAmount")
    private double servingsAmount;

    @Column(name="unit")
    private String unit;
}
