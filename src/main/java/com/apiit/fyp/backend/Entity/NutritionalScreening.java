package com.apiit.fyp.backend.Entity;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "nutritionalScreening")
public class NutritionalScreening {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "nutritionalScreeningId")
    private int nutritionalScreeningId;

    @Column(name = "height")
    private int height;

    @Column(name = "weight")
    private int weight;

    @Column(name = "waist")
    private int waist;

    @Nullable
    @Column(name = "hip")
    private int hip;

    @Nullable
    @Column(name = "PAL")
    private int PAL;

    @Column(name = "BMI")
    private int BMI;

    @Column(name = "IBW")
    private int IBW;

    @Column(name = "BMR")
    private int BMR;

    @Nullable
    @Column(name = "DER")
    private int DER;

    @Nullable
    @Column(name = "date")
    private Date date;

    @Nullable
    @Column(name = "riskFactors")
    private String riskFactors;

    @Nullable
    @Column(name="gender")
    private String gender;

    @Nullable
    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user", referencedColumnName = "userId")
    private User user;



}
