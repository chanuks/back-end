package com.apiit.fyp.backend.Entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "servingCalculation")
public class ServingCalculation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "servingCalculationId")
    private int servingCalculationId;


    @Column(name="cereals")
    private int cereals;

    @Column(name="pulses")
    private int pulses;

    @Column(name="fruitsHighCHO")
    private int fruitsHighCHO;

    @Column(name = "fruitsMediumCHO")
    private int fruitsMediumCHO;

    @Column(name = "fruitsLowCHO")
    private int fruitsLowCHO;

    @Column(name = "vegetablesHighCHO")
    private int vegetablesHighCHO;

    @Column(name = "vegetablesMediumCHO")
    private int vegetablesMediumCHO;

    @Column(name = "vegetablesLowCHO")
    private int vegetablesLowCHO;

    @Column(name = "vegetablesGreenLeafy")
    private int vegetablesGreenLeafy;

    @Column(name = "milk")
    private int milk;

    @Column(name = "meat")
    private int meat;

    @Column(name = "poultry")
    private int poultry;

    @Column(name = "fish")
    private int fish;


    @Column(name = "fatsOils")
    private int fatsOils;

    @Column(name = "nuts")
    private int nuts;

    @Column(name = "coconut")
    private int coconut;

    @Column(name = "choInG")
    private int choInG;

    @Column(name = "proteinInG")
    private int proteinInG;

    @Column(name = "fatInG")
    private int fatInG;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_userId", referencedColumnName = "userId")
    private User user;

}
