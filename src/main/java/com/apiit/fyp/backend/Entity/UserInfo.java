package com.apiit.fyp.backend.Entity;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "userInfo")
public class UserInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userInfoId")
    private int userInfoId;

    @Column(name = "breakfastFood")
    private String breakfastFood;

    @Column(name = "lunchFood")
    private String lunchFood;

    @Column(name = "dinnerFood")
    private String dinnerFood;

    @Column(name = "eatingPattern")
    private String eatingPattern;

    @Column(name = "userAllergies")
    private String[]  userAllergies;

    @Column(name = "userMedicalConditions")
    private String[] userMedicalConditions;

    @Column(name = "DER")
    private int DER;

    @Nullable
    @OneToOne (fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_userId", referencedColumnName = "userId")
    private User user;
//    @OneToOne(mappedBy = "userInfo")
//    private User user;
}
