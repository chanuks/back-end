package com.apiit.fyp.backend.Entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name = "mealServings")
public class MealServings {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "mealServingid")
    private int mealServingid;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "breakfast_id", referencedColumnName = "BreakfastServingsId")
    private BreakfastServings breakfastServings;

     @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "snack1_id", referencedColumnName = "Snack1ServingsId")
    private Snack1Servings snack1Servings;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "lunch_id", referencedColumnName = "LunchServingsId")
    private LunchServings lunchServings;

     @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "snack2_id", referencedColumnName = "Snack2ServingsId")
    private Snack2Servings snack2Servings;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "dinner_id", referencedColumnName = "DinnerServingsId")
    private DinnerServings dinnerServings;

    @ManyToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_userId", referencedColumnName = "userId")
    private User user;


}
