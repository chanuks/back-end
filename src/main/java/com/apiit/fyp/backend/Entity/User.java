package com.apiit.fyp.backend.Entity;

import lombok.*;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "userId")
    private int userId;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;


    @Nullable
    @Column(name = "firstname")
    private String firstname;

    @Nullable
    @Column(name = "lastname")
    private String lastname;

    @Nullable
    @Column(name = "region")
    private String region;

    @Nullable
    @Column(name = "gender")
    private String gender;

    @Nullable
    @Column(name = "birthdate")
    private Date birthdate;

    @Nullable
    @Column(name = "ageGroup")
    private String ageGroup;

    @Nullable
    @Column(name = "category")
    private String category;

    @OneToMany(cascade = CascadeType.ALL,mappedBy="user")
    private List<NutritionalScreening> nutritionalScreenings;

//    @OneToMany(cascade = CascadeType.ALL,mappedBy="user")
//    private List<Meal> meals;
//    @OneToMany(cascade = CascadeType.ALL,mappedBy="user",fetch =FetchType.LAZY )
//    private List<UserInfo> userInfos;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userInfoId", referencedColumnName = "userInfoId")
    private UserInfo userInfo;
//    @OneToMany(mappedBy = "user")
//    private List<MealServings>mealServings;

}
