package com.apiit.fyp.backend.DTO;

import com.apiit.fyp.backend.Entity.UserInfo;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private int userId;
    private String username;
    private String breakfastFood;

}
