package com.apiit.fyp.backend.DTO;

import com.apiit.fyp.backend.Entity.User;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ServingCalculationDTO {


    private int userId;

    private String dietType;

    private int cereals;


    private int pulses;


    private int fruitsHighCHO;


    private int fruitsMediumCHO;


    private int fruitsLowCHO;


    private int vegetablesHighCHO;


    private int vegetablesMediumCHO;

    private int vegetablesLowCHO;


    private int vegetablesGreenLeafy;

    private int milk;


    private int meat;

    private int wheyProtein;

    private int eggs;

    private int fish;


    private int fatsOils;


    private int nuts;


    private int coconut;

    private int choInG;


    private int proteinInG;


    private int fatInG;

    private double requiredCHO;

    private double requiredProtein;

    private double requiredFat;

    private double generatedCHO;

    private double generatedProtein;

    private double generatedFat;

    private User user;
}
