package com.apiit.fyp.backend.DTO;

import com.apiit.fyp.backend.Entity.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealServingsDTO {
    private int mealServingid;
    private BreakfastServings breakfastServings;
    private Snack1Servings snack1Servings;
    private LunchServings lunchServings;
    private Snack2Servings snack2Servings;
    private DinnerServings dinnerServings;
    private int userId;
    private double requiredCHO;

    private double requiredProtein;

    private double requiredFat;

    private double generatedCHO;

    private double generatedProtein;

    private double generatedFat;
}
