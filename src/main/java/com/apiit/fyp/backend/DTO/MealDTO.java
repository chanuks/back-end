package com.apiit.fyp.backend.DTO;

import com.apiit.fyp.backend.Entity.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MealDTO {

    private int mealId;
    private BreakfastFoodList breakfastFoods;
    private Snack1FoodList snack1Foods;
    private LunchFoodList lunchFoods;
    private Snack2FoodList snack2Foods;
    private DinnerFoodList dinnerFoods;
    private int userId;
    private double requiredCHO;

    private double requiredProtein;

    private double requiredFat;

    private double generatedCHO;

    private double generatedProtein;

    private double generatedFat;

}
