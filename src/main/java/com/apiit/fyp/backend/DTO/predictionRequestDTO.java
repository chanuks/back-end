package com.apiit.fyp.backend.DTO;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class predictionRequestDTO {
    private String timestamp;
    private int age_Group;
    private int gender;
    private int location;
    private int household;
    private String breakfast;
    private String lunch;
    private String dinner;

}
