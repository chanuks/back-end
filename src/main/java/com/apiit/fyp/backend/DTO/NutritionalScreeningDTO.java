package com.apiit.fyp.backend.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class NutritionalScreeningDTO {
    private int nutritionalScreeningId;

    private int height;

    private int weight;

    private int waist;

    private int hip;

    private int PAL;

    private String gender;

    private int BMR;

    private int BMI;

    private int IBW;

    private int DER;

    private String riskFactors;

    private Date date;

    private int userId;
}
