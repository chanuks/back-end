package com.apiit.fyp.backend.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserInfoDTO {

//    int userId;
//    int der;
//    String eatingPattern;
//    String[] medicalConditions;
//    String[] allegies;


//    public UserInfoDTO(int userId, int der, String eatingPattern, String lunchFood, String breakfastFood, String dinnerFood, String[] userMedicalConditions, String[] userAllergies) {
//        this.userId = userId;
//        this.der = der;
//        this.eatingPattern = eatingPattern;
//        this.lunchFood = lunchFood;
//        this.breakfastFood = breakfastFood;
//        this.dinnerFood = dinnerFood;
//        this.userMedicalConditions = userMedicalConditions;
//        this.userAllergies = userAllergies;
//    }

    int userId;
    int der;
    String eatingPattern;
    String lunchFood;
    String breakfastFood;
    String dinnerFood;
    String[] userMedicalConditions;
    String[] userAllergies;

}
