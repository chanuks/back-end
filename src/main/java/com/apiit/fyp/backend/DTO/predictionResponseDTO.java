package com.apiit.fyp.backend.DTO;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class predictionResponseDTO {
    private String Timestamp;
    private double Age_Group;
    private double Gender;
    private double Location;
    private double Household;
    private double Breakfast;
    private double Scored_Probabilities_for_Class_Bread;
    private double Scored_Probabilities_for_Class_Cereals;
    private double Scored_Probabilities_for_Class_Grams;
    private double Scored_Probabilities_for_Class_Rice;
    private double Scored_Probabilities_for_Class_Rice_Flour_Foods;
    private double Scored_Probabilities_for_Class_Wheat_Flour_Foods;
    private double Scored_Probabilities_for_Class_Yams;
     private String Scored_Label;
}
