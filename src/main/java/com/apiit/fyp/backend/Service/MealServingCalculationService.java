package com.apiit.fyp.backend.Service;


import com.apiit.fyp.backend.Auth.service.UserService;
import com.apiit.fyp.backend.DTO.MealServingsDTO;
import com.apiit.fyp.backend.DTO.ServingCalculationDTO;
import com.apiit.fyp.backend.Entity.*;
import com.apiit.fyp.backend.Repository.MealServingCalculationDAO;
import com.apiit.fyp.backend.Repository.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MealServingCalculationService {

    @Autowired
    private UserService userServices;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private MealServingCalculationDAO mealServingCalculationDAO;

    public static double roundToHalf(double d) {
        return Math.round(d * 2) / 2.0;
    }


    public MealServingsDTO calculateMealServings (ServingCalculationDTO servingCalculationDTO){



        MealServings newMeals = new MealServings();


        User user = userDAO.getUserByUserId(servingCalculationDTO.getUserId());
        newMeals.setUser(user);





        BreakfastServings breakfast = new BreakfastServings();
        DinnerServings dinner = new DinnerServings();
        LunchServings lunch = new LunchServings();
        Snack1Servings snack1 = new Snack1Servings();
        Snack2Servings snack2 = new Snack2Servings();

        //Breaking down  Cereal Servings


        double Bcereal = Math.round((servingCalculationDTO.getCereals()*0.30) * 2) / 2.0 ;
        double S1cereal = Math.round((servingCalculationDTO.getCereals()*0.10) * 2) / 2.0;
        double Lcereal = Math.round((servingCalculationDTO.getCereals()*0.30) * 2) / 2.0;
        double S2cereal = Math.round((servingCalculationDTO.getCereals()*0.10) * 2) / 2.0 ;
        double Dcereal =  Math.round((servingCalculationDTO.getCereals()*0.20) * 2) / 2.0;


        breakfast.setCereals(Bcereal);
        snack1.setCereals (S1cereal);
        lunch.setCereals(Lcereal);
        snack2.setCereals(S2cereal);
        dinner.setCereals(Dcereal);




        //
//        double Bcereal = servingCalculationDTO.getCereals()*0.30;
//        double S1cereal = servingCalculationDTO.getCereals()*0.10;
//        double Lcereal = servingCalculationDTO.getCereals()*0.30;
//        double S2cereal = servingCalculationDTO.getCereals()*0.10;
//        double Dcereal = servingCalculationDTO.getCereals()*0.20;


        //Pulses

        double Bpulses = Math.round((servingCalculationDTO.getPulses()*0.40) * 2) / 2.0;
        double Lpulses = Math.round((servingCalculationDTO.getPulses()*0.40) * 2) / 2.0;
        double Dpulses = Math.round((servingCalculationDTO.getPulses()*0.20) * 2) / 2.0;


//        double Bpulses = servingCalculationDTO.getPulses()*0.40;
//        double Lpulses = servingCalculationDTO.getPulses()*0.40;
//        double Dpulses = servingCalculationDTO.getPulses()*0.20;
        breakfast.setPulses(Bpulses);
        lunch.setPulses(Lpulses);
        dinner.setPulses(Dpulses);

        //FruitHighCHO

        double S1highCHOfruit =   Math.round((servingCalculationDTO.getFruitsHighCHO()) * 2) / 2.0 ;

//        double S1highCHOfruit = servingCalculationDTO.getFruitsHighCHO();
        snack1.setFruitsHighCHO(S1highCHOfruit);

        //FruitsMediumCHO

        double S2mediumCHOfruit =  Math.round((servingCalculationDTO.getFruitsMediumCHO()) * 2) / 2.0;

//        double S2mediumCHOfruit = servingCalculationDTO.getFruitsMediumCHO();
        snack2.setFruitsMediumCHO(S2mediumCHOfruit);

        //FruitsLowCHO

        double BlowCHOfruit =  Math.round((servingCalculationDTO.getFruitsLowCHO()*0.50) * 2) / 2.0;
        double DlowCHOfruit =    Math.round((servingCalculationDTO.getFruitsLowCHO()*0.50) * 2) / 2.0;

//        double BlowCHOfruit = servingCalculationDTO.getFruitsLowCHO()*0.50;
//        double DlowCHOfruit = servingCalculationDTO.getFruitsLowCHO()*0.50;
        breakfast.setFruitsLowCHO(BlowCHOfruit);
        dinner.setFruitsLowCHO(DlowCHOfruit);


        //vegetableMediumCHO

        double LmediumCHOveg =     Math.round((servingCalculationDTO.getVegetablesMediumCHO()*0.50) * 2) / 2.0;
        double DmediumCHOveg =     Math.round((servingCalculationDTO.getVegetablesMediumCHO()*0.50) * 2) / 2.0;


//        double LmediumCHOveg = servingCalculationDTO.getVegetablesMediumCHO()*0.50;
//        double DmediumCHOveg = servingCalculationDTO.getVegetablesMediumCHO()*0.50;
        lunch.setVegetablesMediumCHO(LmediumCHOveg);
        dinner.setVegetablesMediumCHO(DmediumCHOveg);

        //vegetableLowCHO

        double LlowCHOveg =   Math.round((servingCalculationDTO.getVegetablesLowCHO()*0.50) * 2) / 2.0 ;
        double DlowCHOveg =   Math.round((servingCalculationDTO.getVegetablesLowCHO()*0.50) * 2) / 2.0;

//        double LlowCHOveg = servingCalculationDTO.getVegetablesLowCHO()*0.50;
//        double DlowCHOveg = servingCalculationDTO.getVegetablesLowCHO()*0.50;
        lunch.setVegetablesLowCHO(LlowCHOveg);
        dinner.setVegetablesLowCHO(DlowCHOveg);


        //greenleafy

        double Lgreenveg =   Math.round((servingCalculationDTO.getVegetablesGreenLeafy()*0.50) * 2) / 2.0;
        double Dgreenveg = Math.round((servingCalculationDTO.getVegetablesGreenLeafy()*0.50) * 2) / 2.0;

//        double Lgreenveg = servingCalculationDTO.getVegetablesGreenLeafy()*0.50;
//        double Dgreenveg = servingCalculationDTO.getVegetablesGreenLeafy()*0.50;
        lunch.setVegetablesGreenLeafy(Lgreenveg);
        dinner.setVegetablesGreenLeafy(Dgreenveg);

        //meat

        double Bmeat =    Math.round((servingCalculationDTO.getMeat()*0.20) * 2) / 2.0;
        double Lmeat =    Math.round((servingCalculationDTO.getMeat()*0.40) * 2) / 2.0;
        double Dmeat =    Math.round((servingCalculationDTO.getMeat()*0.40) * 2) / 2.0;


//        double Bmeat = servingCalculationDTO.getMeat()*0.20;
//        double Lmeat = servingCalculationDTO.getMeat()*0.40;
//        double Dmeat = servingCalculationDTO.getMeat()*0.40;
        breakfast.setMeatHighFat(Bmeat);
        lunch.setMeatHighFat(Lmeat);
        dinner.setMeatHighFat(Dmeat);


        //For all meals - fats,nuts,coconut
        breakfast.setFatsOils(Math.round((servingCalculationDTO.getFatsOils()) * 2) / 2.0);
        breakfast.setMilkFresh(Math.round((servingCalculationDTO.getMilk()) * 2) / 2.0);
        breakfast.setNuts(Math.round((servingCalculationDTO.getNuts()) * 2) / 2.0);
        breakfast.setCoconut( Math.round((servingCalculationDTO.getCoconut()) * 2) / 2.0);
        breakfast.setEgg( Math.round((servingCalculationDTO.getEggs()) * 2) / 2.0);

        newMeals.setBreakfastServings(breakfast);
        newMeals.setDinnerServings(dinner);
        newMeals.setLunchServings(lunch);
        newMeals.setSnack1Servings(snack1);
        newMeals.setSnack2Servings(snack2);

//        mealServingCalculationDAO.save(newMeals);

        MealServingsDTO mealServingsDTO = new MealServingsDTO();
        mealServingsDTO.setBreakfastServings(newMeals.getBreakfastServings());
        mealServingsDTO.setSnack1Servings(newMeals.getSnack1Servings());
        mealServingsDTO.setLunchServings(newMeals.getLunchServings());
        mealServingsDTO.setSnack2Servings(newMeals.getSnack2Servings());
        mealServingsDTO.setDinnerServings(newMeals.getDinnerServings());
        mealServingsDTO.setUserId(newMeals.getUser().getUserId());
        mealServingsDTO.setRequiredCHO(servingCalculationDTO.getRequiredCHO());
        mealServingsDTO.setRequiredFat(servingCalculationDTO.getRequiredFat());
        mealServingsDTO.setRequiredProtein(servingCalculationDTO.getRequiredProtein());
        mealServingsDTO.setGeneratedCHO(servingCalculationDTO.getGeneratedCHO());
        mealServingsDTO.setGeneratedFat(servingCalculationDTO.getGeneratedFat());
        mealServingsDTO.setGeneratedProtein(servingCalculationDTO.getGeneratedProtein());


        return  mealServingsDTO;
       // return newMeals;

    }
}
