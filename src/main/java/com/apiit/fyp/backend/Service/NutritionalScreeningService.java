package com.apiit.fyp.backend.Service;

import com.apiit.fyp.backend.DTO.NutritionalScreeningDTO;
import com.apiit.fyp.backend.Entity.NutritionalScreening;
import com.apiit.fyp.backend.Repository.NutritionalScreeningDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class NutritionalScreeningService {

    @Autowired
    private NutritionalScreeningDAO nutritionalScreeningDAO;

public NutritionalScreening save(NutritionalScreening nutritionalScreening){
    NutritionalScreening newNutritionalScreening = new NutritionalScreening();
    newNutritionalScreening.setWeight(nutritionalScreening.getWeight());
    newNutritionalScreening.setHeight(nutritionalScreening.getHeight());
    newNutritionalScreening.setWaist(nutritionalScreening.getWaist());
    newNutritionalScreening.setHip(nutritionalScreening.getHip());
    newNutritionalScreening.setPAL(nutritionalScreening.getPAL());
    newNutritionalScreening.setIBW(nutritionalScreening.getIBW());
    newNutritionalScreening.setBMI(nutritionalScreening.getBMI());
    newNutritionalScreening.setBMR(nutritionalScreening.getBMR());
    newNutritionalScreening.setDER(nutritionalScreening.getDER());
    newNutritionalScreening.setDate(nutritionalScreening.getDate());
    newNutritionalScreening.setGender(nutritionalScreening.getGender());
    newNutritionalScreening.setRiskFactors(nutritionalScreening.getRiskFactors());
    newNutritionalScreening.setUser(nutritionalScreening.getUser());

    return nutritionalScreeningDAO.save(newNutritionalScreening);



}

public List<NutritionalScreeningDTO> getNutritionalScreenings(int userid){
    List<NutritionalScreening> nutritionalScreenings = nutritionalScreeningDAO.findAll();
    List<NutritionalScreeningDTO> list = new ArrayList<>();

    nutritionalScreenings.forEach(nutritionalScreening -> {
        NutritionalScreeningDTO dto = new NutritionalScreeningDTO();
        dto.setNutritionalScreeningId(nutritionalScreening.getNutritionalScreeningId());
        dto.setHeight(nutritionalScreening.getHeight());
        dto.setWeight(nutritionalScreening.getWeight());
        dto.setWaist(nutritionalScreening.getWaist());
        dto.setHip(nutritionalScreening.getHip());
        dto.setPAL(nutritionalScreening.getPAL());
        dto.setIBW(nutritionalScreening.getIBW());
        dto.setBMI(nutritionalScreening.getBMI());
        dto.setBMR(nutritionalScreening.getBMR());
        dto.setDate(nutritionalScreening.getDate());
        dto.setGender(nutritionalScreening.getUser().getGender());
        dto.setRiskFactors(nutritionalScreening.getRiskFactors());
        dto.setUserId(nutritionalScreening.getUser().getUserId());

        if (dto.getUserId() == userid) {
            list.add(dto);
        }
    });
    return list;
}

}
