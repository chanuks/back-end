package com.apiit.fyp.backend.Service;


import com.apiit.fyp.backend.AzureBreakfastPredictionAPI;
import com.apiit.fyp.backend.AzureDinnerPredictionAPI;
import com.apiit.fyp.backend.AzureLunchPredictionAPI;
import com.apiit.fyp.backend.DTO.MealDTO;
import com.apiit.fyp.backend.DTO.MealServingsDTO;
import com.apiit.fyp.backend.DTO.predictionRequestDTO;
import com.apiit.fyp.backend.Entity.*;
import com.apiit.fyp.backend.Repository.FoodExchangeDAO;
import com.apiit.fyp.backend.Repository.MealDAO;
import com.apiit.fyp.backend.Repository.UserDAO;
import com.apiit.fyp.backend.Repository.UserInfoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Service
@Transactional
public class MealGenerationService {

    @Autowired
    private FoodExchangeDAO foodExchangeDAO;

    @Autowired
    private MealDAO mealDAO;

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserInfoDAO userInfoDAO;

    @Autowired
    private AzureBreakfastPredictionAPI BreakfastPredictionAPI;

    @Autowired
    private AzureLunchPredictionAPI LunchPredictionAPI;

    @Autowired
    private AzureDinnerPredictionAPI DinnerPredictionAPI;


    public List<FoodExchange> getFoodByGroup(String group){

       return foodExchangeDAO.findByFoodGroup(group);
    }

    public List<FoodExchange> getFoodByType(String type){

        return foodExchangeDAO.findByFoodType(type);
    }

    public LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
        return dateToConvert.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
    }

    public MealDTO generateMeal(MealServingsDTO mealServings) {


        //Serving amounts for each meal
        BreakfastServings breakfastServings = mealServings.getBreakfastServings();

        Snack1Servings snack1Servings = mealServings.getSnack1Servings();

        LunchServings lunchServings = mealServings.getLunchServings();

        Snack2Servings snack2Servings = mealServings.getSnack2Servings();

        DinnerServings dinnerServings = mealServings.getDinnerServings();


        //Foods from each food group from the exchange table

        List<FoodExchange> cereals = getFoodByGroup("Cereals");

        List<FoodExchange> snacks = getFoodByGroup("Snacks");

        List<FoodExchange> pulses = getFoodByGroup("Pulses");

        List<FoodExchange> high_cho_fruits = getFoodByGroup("High CHO Fruits");

        List<FoodExchange> medium_cho_fruits = getFoodByGroup("Medium CHO Fruits");

        List<FoodExchange> low_cho_fruits = getFoodByGroup("Low CHO Fruits");

        List<FoodExchange> high_cho_vegetables = getFoodByGroup("High CHO Vegetables");

        List<FoodExchange> medium_cho_vegetables = getFoodByGroup("Medium CHO Vegetables");

        List<FoodExchange> low_cho_vegetables = getFoodByGroup("Low CHO Vegetables");

        List<FoodExchange> greeny_leafy_vegetables = getFoodByGroup("Greeny Leafy Vegetables");

        List<FoodExchange> milk = getFoodByGroup("Milk");

        List<FoodExchange> meat = getFoodByGroup("Meat");

        List<FoodExchange> fats = getFoodByGroup("Fats & Oils");

        List<FoodExchange> coconut = getFoodByGroup("Coconut");


        List<FoodExchange> rice = getFoodByType("Rice");
        List<FoodExchange> bread = getFoodByType("Bread");
        List<FoodExchange> rice_flour_foods = getFoodByType("Rice flour based foods");
        List<FoodExchange> wheat_flour_foods = getFoodByType("Wheat flour based foods");
        List<FoodExchange> yams = getFoodByType("Yams");
        List<FoodExchange> grams = getFoodByType("Grams");
        List<FoodExchange> cereal = getFoodByType("Cereals");

        User user = userDAO.getUserByUserId(mealServings.getUserId());
        UserInfo userInfo = userInfoDAO.getByUserUserId(mealServings.getUserId());


        String breakfastFood = userInfo.getBreakfastFood();
        String lunchFood = userInfo.getLunchFood();
        String dinnerFood = userInfo.getDinnerFood();
        Date birthdate = user.getBirthdate();
        String category = user.getCategory();
        String region = user.getRegion();
        String gender = user.getGender();

        predictionRequestDTO request = new predictionRequestDTO();

        // Age to Age group transformation

        LocalDate birth = convertToLocalDateViaInstant(birthdate);

        LocalDate today = LocalDate.now();                          //Today's date
        LocalDate birthday = birth;  //Birth date

        Period p = Period.between(birthday, today);

        int ageInYears = p.getYears();

        if (ageInYears <= 12) {
            request.setAge_Group(1);
        }
        if (ageInYears <= 17 && ageInYears > 12) {
            request.setAge_Group(2);
        }
        if (ageInYears <= 24 && ageInYears > 18) {
            request.setAge_Group(3);
        }
        if (ageInYears <= 34 && ageInYears > 25) {
            request.setAge_Group(4);
        }
        if (ageInYears <= 44 && ageInYears > 35) {
            request.setAge_Group(5);
        }
        if (ageInYears <= 54 && ageInYears > 45) {
            request.setAge_Group(5);
        }
        if (ageInYears <= 64 && ageInYears > 55) {
            request.setAge_Group(5);
        }
        if (ageInYears <= 74 && ageInYears > 65) {
            request.setAge_Group(5);
        }
        if (ageInYears >= 75) {
            request.setAge_Group(5);
        }

        // Gender numerical transformation

        if (gender.contains("Male")) {
            request.setGender(1);
        }
        if (gender.contains("Female")) {
            request.setGender(2);
        }

        //Location numerical transformation

        if (region.contains("Colombo")) {
            request.setLocation(1);
        }
        if (region.contains("Kalutara")) {
            request.setLocation(2);
        }
        if (region.contains("Gampaha")) {
            request.setLocation(3);
        }
        if (region.contains("Matale")) {
            request.setLocation(4);
        }
        if (region.contains("Kandy")) {
            request.setLocation(5);
        }
        if (region.contains("Nuwara Eliya")) {
            request.setLocation(6);
        }
        if (region.contains("Kegalle")) {
            request.setLocation(7);
        }
        if (region.contains("Ratnapura")) {
            request.setLocation(8);
        }
        if (region.contains("Anuradhapura")) {
            request.setLocation(9);
        }
        if (region.contains("Polonnaruwa")) {
            request.setLocation(10);
        }
        if (region.contains("Hambantota")) {
            request.setLocation(11);
        }
        if (region.contains("Matara")) {
            request.setLocation(12);
        }
        if (region.contains("Galle")) {
            request.setLocation(13);
        }
        if (region.contains("Badulla")) {
            request.setLocation(14);
        }
        if (region.contains("Monaragala")) {
            request.setLocation(15);
        }
        if (region.contains("Trincomalee")) {
            request.setLocation(16);
        }
        if (region.contains("Batticaloa")) {
            request.setLocation(17);
        }
        if (region.contains("Ampara")) {
            request.setLocation(18);
        }
        if (region.contains("Kurunegala")) {
            request.setLocation(19);
        }
        if (region.contains("Puttalam")) {
            request.setLocation(20);
        }
        if (region.contains("Jaffna")) {
            request.setLocation(21);
        }
        if (region.contains("Kilinochchi")) {
            request.setLocation(22);
        }
        if (region.contains("Mannar")) {
            request.setLocation(23);
        }
        if (region.contains("Mullaitivu")) {
            request.setLocation(24);
        }
        if (region.contains("Vavuniya")) {
            request.setLocation(25);
        }


        //Household transformation

        if (category.contains("Engaging in studies")) {
            request.setHousehold(3);
        }
        if (category.contains("Staying at home")) {
            request.setHousehold(1);
        }
        if (category.contains("Engaging in a job")) {
            request.setHousehold(2);
        }


        request.setBreakfast(breakfastFood);
        request.setLunch(lunchFood);
        request.setDinner(dinnerFood);


        String breakfastPrediction = BreakfastPredictionAPI.getBreakfastPrediction(request);
        String lunchPrediction = LunchPredictionAPI.getLunchPrediction(request);
        String dinnerPrediction = DinnerPredictionAPI.getDinnerPrediction(request);


        Meal meal = new Meal();
        BreakfastFoodList breakfast = new BreakfastFoodList();
        Snack1FoodList snack1 = new Snack1FoodList();
        LunchFoodList lunch = new LunchFoodList();
        Snack2FoodList snack2 = new Snack2FoodList();
        DinnerFoodList dinner = new DinnerFoodList();


        Random rand = new Random();

        //Generating Breakfast Menu

        List<Food> breakfastList = new ArrayList<>();

        //Breakfast Cereal
         FoodExchange B_cerealExchange = null;

        if (breakfastPrediction.contains("Rice")) {
           B_cerealExchange = rice.get(rand.nextInt(rice.size()));
        }
        if (breakfastPrediction.contains("Bread")) {
            B_cerealExchange = bread.get(rand.nextInt(bread.size()));
        }
        if (breakfastPrediction.contains("Cereals")) {
             B_cerealExchange = cereal.get(rand.nextInt(cereal.size()));
        }
        if (breakfastPrediction.contains("Grams")) {
             B_cerealExchange = grams.get(rand.nextInt(grams.size()));
        }
        if (breakfastPrediction.contains("Rice Flour Foods")) {
             B_cerealExchange = rice_flour_foods.get(rand.nextInt(rice_flour_foods.size()));
        }
        if (breakfastPrediction.contains("Wheat Flour Foods")) {
            B_cerealExchange = wheat_flour_foods.get(rand.nextInt(wheat_flour_foods.size()));
        }
        if (breakfastPrediction.contains("Yams")) {
            B_cerealExchange = yams.get(rand.nextInt(yams.size()));
        }

//        FoodExchange B_cerealExchange = cereals.get(rand.nextInt(cereals.size()));

//        FoodExchange B_cerealExchangePrediction = rice.get(rand.nextInt(rice.size()));

        Food B_cereal = new Food();
        B_cereal.setFood(B_cerealExchange.getFood());
        B_cereal.setFoodGroup(B_cerealExchange.getFoodGroup());
        B_cereal.setServingsAmount((breakfastServings.getCereals()) * (B_cerealExchange.getAmountPerServing()));
        B_cereal.setUnit(B_cerealExchange.getUnit());

        breakfastList.add(B_cereal);

        cereals.remove(B_cerealExchange);


        //Breakfast Pulse

        FoodExchange B_pulseExchange = pulses.get(rand.nextInt(pulses.size()));

        Food B_pulse = new Food();
        B_pulse.setFood(B_pulseExchange.getFood());
        B_pulse.setFoodGroup(B_pulseExchange.getFoodGroup());
        B_pulse.setServingsAmount((breakfastServings.getPulses()) * (B_pulseExchange.getAmountPerServing()));
        B_pulse.setUnit(B_pulseExchange.getUnit());

        breakfastList.add(B_pulse);

        pulses.remove(B_pulseExchange);


        //Breakfast fruitsHighCHO

        if (breakfastServings.getFruitsHighCHO() != 0.0) {
            FoodExchange B_fruitHighExchange = high_cho_fruits.get(rand.nextInt(high_cho_fruits.size()));

            Food B_fruitHigh = new Food();
            B_fruitHigh.setFood(B_fruitHighExchange.getFood());
            B_fruitHigh.setFoodGroup(B_fruitHighExchange.getFoodGroup());
            B_fruitHigh.setServingsAmount((breakfastServings.getFruitsHighCHO()) * (B_fruitHighExchange.getAmountPerServing()));
            B_fruitHigh.setUnit(B_fruitHighExchange.getUnit());

            breakfastList.add(B_fruitHigh);

            high_cho_fruits.remove(B_fruitHighExchange);
        }


        //Breakfast fruitsMediumCHO

        if (breakfastServings.getFruitsMediumCHO() != 0.0) {
            FoodExchange B_fruitMediumExchange = medium_cho_fruits.get(rand.nextInt(medium_cho_fruits.size()));

            Food B_fruitMedium = new Food();
            B_fruitMedium.setFood(B_fruitMediumExchange.getFood());
            B_fruitMedium.setFoodGroup(B_fruitMediumExchange.getFoodGroup());
            B_fruitMedium.setServingsAmount((breakfastServings.getFruitsMediumCHO()) * (B_fruitMediumExchange.getAmountPerServing()));
            B_fruitMedium.setUnit(B_fruitMediumExchange.getUnit());

            breakfastList.add(B_fruitMedium);

            medium_cho_fruits.remove(B_fruitMediumExchange);
        }


        //Breakfast fruitsLowCHO

        if (breakfastServings.getFruitsLowCHO() != 0.0) {
            FoodExchange B_fruitLowExchange = low_cho_fruits.get(rand.nextInt(low_cho_fruits.size()));

            Food B_fruitLow = new Food();
            B_fruitLow.setFood(B_fruitLowExchange.getFood());
            B_fruitLow.setFoodGroup(B_fruitLowExchange.getFoodGroup());
            B_fruitLow.setServingsAmount((breakfastServings.getFruitsLowCHO()) * (B_fruitLowExchange.getAmountPerServing()));
            B_fruitLow.setUnit(B_fruitLowExchange.getUnit());

            breakfastList.add(B_fruitLow);

            low_cho_fruits.remove(B_fruitLowExchange);
        }

        //Breakfast vegetablesHighCHO

        if (breakfastServings.getVegetablesHighCHO() != 0.0) {
            FoodExchange B_vegetableHighExchange = high_cho_vegetables.get(rand.nextInt(high_cho_vegetables.size()));

            Food B_vegetableHigh = new Food();
            B_vegetableHigh.setFood(B_vegetableHighExchange.getFood());
            B_vegetableHigh.setFoodGroup(B_vegetableHighExchange.getFoodGroup());
            B_vegetableHigh.setServingsAmount((breakfastServings.getVegetablesHighCHO()) * (B_vegetableHighExchange.getAmountPerServing()));
            B_vegetableHigh.setUnit(B_vegetableHighExchange.getUnit());

            breakfastList.add(B_vegetableHigh);

            high_cho_vegetables.remove(B_vegetableHighExchange);
        }


        //Breakfast vegetablesMediumCHO

        if (breakfastServings.getVegetablesMediumCHO() != 0.0) {
            FoodExchange B_vegetableMediumExchange = medium_cho_vegetables.get(rand.nextInt(medium_cho_vegetables.size()));

            Food B_vegetableMedium = new Food();
            B_vegetableMedium.setFood(B_vegetableMediumExchange.getFood());
            B_vegetableMedium.setFoodGroup(B_vegetableMediumExchange.getFoodGroup());
            B_vegetableMedium.setServingsAmount((breakfastServings.getVegetablesMediumCHO()) * (B_vegetableMediumExchange.getAmountPerServing()));
            B_vegetableMedium.setUnit(B_vegetableMediumExchange.getUnit());

            breakfastList.add(B_vegetableMedium);

            medium_cho_vegetables.remove(B_vegetableMediumExchange);
        }

        //Breakfast vegetablesLowCHO
        if (breakfastServings.getVegetablesLowCHO() != 0.0) {
            FoodExchange B_vegetableLowExchange = low_cho_vegetables.get(rand.nextInt(low_cho_vegetables.size()));

            Food B_vegetableLow = new Food();
            B_vegetableLow.setFood(B_vegetableLowExchange.getFood());
            B_vegetableLow.setFoodGroup(B_vegetableLowExchange.getFoodGroup());
            B_vegetableLow.setServingsAmount((breakfastServings.getVegetablesLowCHO()) * (B_vegetableLowExchange.getAmountPerServing()));
            B_vegetableLow.setUnit(B_vegetableLowExchange.getUnit());

            breakfastList.add(B_vegetableLow);

            low_cho_vegetables.remove(B_vegetableLowExchange);
        }
        //Breakfast vegetablesGreenLeafy

        if (breakfastServings.getVegetablesGreenLeafy() != 0.0) {
            FoodExchange B_vegetableGrenLeafyExchange = greeny_leafy_vegetables.get(rand.nextInt(greeny_leafy_vegetables.size()));

            Food B_vegetableGreen = new Food();
            B_vegetableGreen.setFood(B_vegetableGrenLeafyExchange.getFood());
            B_vegetableGreen.setFoodGroup(B_vegetableGrenLeafyExchange.getFoodGroup());
            B_vegetableGreen.setServingsAmount((breakfastServings.getVegetablesGreenLeafy()) * (B_vegetableGrenLeafyExchange.getAmountPerServing()));
            B_vegetableGreen.setUnit(B_vegetableGrenLeafyExchange.getUnit());

            breakfastList.add(B_vegetableGreen);

            greeny_leafy_vegetables.remove(B_vegetableGrenLeafyExchange);
        }

        //Breakfast milk

        if (breakfastServings.getMilkFresh() != 0.0) {
            FoodExchange B_milkExchange = milk.get(rand.nextInt(milk.size()));

            Food B_milk = new Food();
            B_milk.setFood(B_milkExchange.getFood());
            B_milk.setFoodGroup(B_milkExchange.getFoodGroup());
            B_milk.setServingsAmount((breakfastServings.getMilkFresh()) * (B_milkExchange.getAmountPerServing()));
            B_milk.setUnit(B_milkExchange.getUnit());

            breakfastList.add(B_milk);

            milk.remove(B_milkExchange);
        }

        //Breakfast meat

        if (breakfastServings.getMeatHighFat() != 0.0) {
            FoodExchange B_meatExchange = meat.get(rand.nextInt(meat.size()));

            Food B_meat = new Food();
            B_meat.setFood(B_meatExchange.getFood());
            B_meat.setFoodGroup(B_meatExchange.getFoodGroup());
            B_meat.setServingsAmount((breakfastServings.getMeatHighFat()) * (B_meatExchange.getAmountPerServing()));
            B_meat.setUnit(B_meatExchange.getUnit());

            breakfastList.add(B_meat);

            meat.remove(B_meatExchange);
        }

        //Whole day - Eggs Portion
        if (breakfastServings.getEgg() != 0.0) {

            Food egg = new Food();
            egg.setFood("Egg");
            egg.setFoodGroup("Egg");
            egg.setServingsAmount((breakfastServings.getEgg()) * (1));
            egg.setUnit("Medium Size");

            breakfastList.add(egg);
        }

        //Whole day - fatsOils
        if (breakfastServings.getFatsOils() != 0.0) {
            FoodExchange B_fatExchange = fats.get(rand.nextInt(fats.size()));

            Food B_fat = new Food();
            B_fat.setFood(B_fatExchange.getFood());
            B_fat.setFoodGroup(B_fatExchange.getFoodGroup());
            B_fat.setServingsAmount((breakfastServings.getFatsOils()) * (B_fatExchange.getAmountPerServing()));
            B_fat.setUnit(B_fatExchange.getUnit());

            breakfastList.add(B_fat);
        }

        //Whole day - Coconut
        if (breakfastServings.getCoconut() != 0.0) {
            FoodExchange B_coconutExchange = coconut.get(rand.nextInt(coconut.size()));

            Food B_coconut = new Food();
            B_coconut.setFood(B_coconutExchange.getFood());
            B_coconut.setFoodGroup(B_coconutExchange.getFoodGroup());
            B_coconut.setServingsAmount((breakfastServings.getCoconut()) * (B_coconutExchange.getAmountPerServing()));
            B_coconut.setUnit(B_coconutExchange.getUnit());

            breakfastList.add(B_coconut);
        }


        //Generating Snack 1 Menu

        List<Food> snack1List = new ArrayList<>();

        //Snack 1 Cereal

        if (snack1Servings.getCereals() != 0.0) {
            FoodExchange S1_cerealExchange = snacks.get(rand.nextInt(snacks.size()));

            Food S1_cereal = new Food();
            S1_cereal.setFood(S1_cerealExchange.getFood());
            S1_cereal.setFoodGroup(S1_cerealExchange.getFoodGroup());
            S1_cereal.setServingsAmount((snack1Servings.getCereals()) * (S1_cerealExchange.getAmountPerServing()));
            S1_cereal.setUnit(S1_cerealExchange.getUnit());

            snack1List.add(S1_cereal);
        }


        //Snack 1 Pulse

        if (snack1Servings.getPulses() != 0.0) {
            FoodExchange S1_pulseExchange = pulses.get(rand.nextInt(pulses.size()));

            Food S1_pulse = new Food();
            S1_pulse.setFood(S1_pulseExchange.getFood());
            S1_pulse.setFoodGroup(S1_pulseExchange.getFoodGroup());
            S1_pulse.setServingsAmount((snack1Servings.getPulses()) * (S1_pulseExchange.getAmountPerServing()));
            S1_pulse.setUnit(S1_pulseExchange.getUnit());

            snack1List.add(S1_pulse);
        }


        //Snack 1 fruitsHighCHO

        if (snack1Servings.getFruitsHighCHO() != 0.0) {
            FoodExchange S1_fruitHighExchange = high_cho_fruits.get(rand.nextInt(high_cho_fruits.size()));

            Food S1_fruitHigh = new Food();
            S1_fruitHigh.setFood(S1_fruitHighExchange.getFood());
            S1_fruitHigh.setFoodGroup(S1_fruitHighExchange.getFoodGroup());
            S1_fruitHigh.setServingsAmount((snack1Servings.getFruitsHighCHO()) * (S1_fruitHighExchange.getAmountPerServing()));
            S1_fruitHigh.setUnit(S1_fruitHighExchange.getUnit());

            snack1List.add(S1_fruitHigh);
        }


        //Snack 1 fruitsMediumCHO

        if (snack1Servings.getFruitsMediumCHO() != 0.0) {
            FoodExchange S1_fruitMediumExchange = medium_cho_fruits.get(rand.nextInt(medium_cho_fruits.size()));

            Food S1_fruitMedium = new Food();
            S1_fruitMedium.setFood(S1_fruitMediumExchange.getFood());
            S1_fruitMedium.setFoodGroup(S1_fruitMediumExchange.getFoodGroup());
            S1_fruitMedium.setServingsAmount((snack1Servings.getFruitsMediumCHO()) * (S1_fruitMediumExchange.getAmountPerServing()));
            S1_fruitMedium.setUnit(S1_fruitMediumExchange.getUnit());

            snack1List.add(S1_fruitMedium);
        }


        //Snack 1 fruitsLowCHO

        if (snack1Servings.getFruitsLowCHO() != 0.0) {
            FoodExchange S1_fruitLowExchange = low_cho_fruits.get(rand.nextInt(low_cho_fruits.size()));

            Food S1_fruitLow = new Food();
            S1_fruitLow.setFood(S1_fruitLowExchange.getFood());
            S1_fruitLow.setFoodGroup(S1_fruitLowExchange.getFoodGroup());
            S1_fruitLow.setServingsAmount((snack1Servings.getFruitsLowCHO()) * (S1_fruitLowExchange.getAmountPerServing()));
            S1_fruitLow.setUnit(S1_fruitLowExchange.getUnit());

            snack1List.add(S1_fruitLow);
        }

        //Snack 1 vegetablesHighCHO

        if (snack1Servings.getVegetablesHighCHO() != 0.0) {
            FoodExchange S1_vegetableHighExchange = high_cho_vegetables.get(rand.nextInt(high_cho_vegetables.size()));

            Food S1_vegetableHigh = new Food();
            S1_vegetableHigh.setFood(S1_vegetableHighExchange.getFood());
            S1_vegetableHigh.setFoodGroup(S1_vegetableHighExchange.getFoodGroup());
            S1_vegetableHigh.setServingsAmount((snack1Servings.getVegetablesHighCHO()) * (S1_vegetableHighExchange.getAmountPerServing()));
            S1_vegetableHigh.setUnit(S1_vegetableHighExchange.getUnit());

            snack1List.add(S1_vegetableHigh);
        }


        //Snack 1 vegetablesMediumCHO

        if (snack1Servings.getVegetablesMediumCHO() != 0.0) {
            FoodExchange S1_vegetableMediumExchange = medium_cho_vegetables.get(rand.nextInt(medium_cho_vegetables.size()));

            Food S1_vegetableMedium = new Food();
            S1_vegetableMedium.setFood(S1_vegetableMediumExchange.getFood());
            S1_vegetableMedium.setFoodGroup(S1_vegetableMediumExchange.getFoodGroup());
            S1_vegetableMedium.setServingsAmount((snack1Servings.getVegetablesMediumCHO()) * (S1_vegetableMediumExchange.getAmountPerServing()));
            S1_vegetableMedium.setUnit(S1_vegetableMediumExchange.getUnit());

            snack1List.add(S1_vegetableMedium);
        }

        //Snack 1 vegetablesLowCHO
        if (snack1Servings.getVegetablesLowCHO() != 0.0) {
            FoodExchange S1_vegetableLowExchange = low_cho_vegetables.get(rand.nextInt(low_cho_vegetables.size()));

            Food S1_vegetableLow = new Food();
            S1_vegetableLow.setFood(S1_vegetableLowExchange.getFood());
            S1_vegetableLow.setFoodGroup(S1_vegetableLowExchange.getFoodGroup());
            S1_vegetableLow.setServingsAmount((snack1Servings.getVegetablesLowCHO()) * (S1_vegetableLowExchange.getAmountPerServing()));
            S1_vegetableLow.setUnit(S1_vegetableLowExchange.getUnit());

            snack1List.add(S1_vegetableLow);
        }
        //Snack 1 vegetablesGreenLeafy

        if (snack1Servings.getVegetablesGreenLeafy() != 0.0) {
            FoodExchange S1_vegetableGrenLeafyExchange = greeny_leafy_vegetables.get(rand.nextInt(greeny_leafy_vegetables.size()));

            Food S1_vegetableGreen = new Food();
            S1_vegetableGreen.setFood(S1_vegetableGrenLeafyExchange.getFood());
            S1_vegetableGreen.setFoodGroup(S1_vegetableGrenLeafyExchange.getFoodGroup());
            S1_vegetableGreen.setServingsAmount((snack1Servings.getVegetablesGreenLeafy()) * (S1_vegetableGrenLeafyExchange.getAmountPerServing()));
            S1_vegetableGreen.setUnit(S1_vegetableGrenLeafyExchange.getUnit());

            snack1List.add(S1_vegetableGreen);
        }

        //Snack 1 milk

        if (snack1Servings.getMilkFresh() != 0.0) {
            FoodExchange S1_milkExchange = milk.get(rand.nextInt(milk.size()));

            Food S1_milk = new Food();
            S1_milk.setFood(S1_milkExchange.getFood());
            S1_milk.setFoodGroup(S1_milkExchange.getFoodGroup());
            S1_milk.setServingsAmount((snack1Servings.getMilkFresh()) * (S1_milkExchange.getAmountPerServing()));
            S1_milk.setUnit(S1_milkExchange.getUnit());

            snack1List.add(S1_milk);
        }

        //Snack 1 meat

        if (snack1Servings.getMeatHighFat() != 0.0) {
            FoodExchange S1_meatExchange = meat.get(rand.nextInt(meat.size()));

            Food S1_meat = new Food();
            S1_meat.setFood(S1_meatExchange.getFood());
            S1_meat.setFoodGroup(S1_meatExchange.getFoodGroup());
            S1_meat.setServingsAmount((snack1Servings.getMeatHighFat()) * (S1_meatExchange.getAmountPerServing()));
            S1_meat.setUnit(S1_meatExchange.getUnit());

            snack1List.add(S1_meat);
        }


        //Generating Lunch Menu

        List<Food> lunchList = new ArrayList<>();

        //Lunch Cereal

        if (lunchServings.getCereals() != 0.0) {

            FoodExchange L_cerealExchange = null;

            if (lunchPrediction.contains("Rice")) {
                L_cerealExchange = rice.get(rand.nextInt(rice.size()));
            }
            if (lunchPrediction.contains("Bread")) {
                L_cerealExchange = bread.get(rand.nextInt(bread.size()));
            }

            if (lunchPrediction.contains("Grams")) {
                L_cerealExchange = grams.get(rand.nextInt(grams.size()));
            }
            if (lunchPrediction.contains("Wheat Flour Foods")) {
                L_cerealExchange = wheat_flour_foods.get(rand.nextInt(wheat_flour_foods.size()));
            }



//            FoodExchange L_cerealExchange = cereals.get(rand.nextInt(cereals.size()));

            Food L_cereal = new Food();
            L_cereal.setFood(L_cerealExchange.getFood());
            L_cereal.setFoodGroup(L_cerealExchange.getFoodGroup());
            L_cereal.setServingsAmount((lunchServings.getCereals()) * (L_cerealExchange.getAmountPerServing()));
            L_cereal.setUnit(L_cerealExchange.getUnit());

            lunchList.add(L_cereal);

            cereals.remove(L_cerealExchange);
        }


        //Lunch Pulse

        if (lunchServings.getPulses() != 0.0) {
            FoodExchange L_pulseExchange = pulses.get(rand.nextInt(pulses.size()));

            Food L_pulse = new Food();
            L_pulse.setFood(L_pulseExchange.getFood());
            L_pulse.setFoodGroup(L_pulseExchange.getFoodGroup());
            L_pulse.setServingsAmount((lunchServings.getPulses()) * (L_pulseExchange.getAmountPerServing()));
            L_pulse.setUnit(L_pulseExchange.getUnit());

            lunchList.add(L_pulse);

            pulses.remove(L_pulseExchange);
        }


        //Lunch fruitsHighCHO

        if (lunchServings.getFruitsHighCHO() != 0.0) {
            FoodExchange L_fruitHighExchange = high_cho_fruits.get(rand.nextInt(high_cho_fruits.size()));

            Food L_fruitHigh = new Food();
            L_fruitHigh.setFood(L_fruitHighExchange.getFood());
            L_fruitHigh.setFoodGroup(L_fruitHighExchange.getFoodGroup());
            L_fruitHigh.setServingsAmount((lunchServings.getFruitsHighCHO()) * (L_fruitHighExchange.getAmountPerServing()));
            L_fruitHigh.setUnit(L_fruitHighExchange.getUnit());

            lunchList.add(L_fruitHigh);
        }


        //Lunch fruitsMediumCHO

        if (lunchServings.getFruitsMediumCHO() != 0.0) {
            FoodExchange L_fruitMediumExchange = medium_cho_fruits.get(rand.nextInt(medium_cho_fruits.size()));

            Food L_fruitMedium = new Food();
            L_fruitMedium.setFood(L_fruitMediumExchange.getFood());
            L_fruitMedium.setFoodGroup(L_fruitMediumExchange.getFoodGroup());
            L_fruitMedium.setServingsAmount((lunchServings.getFruitsMediumCHO()) * (L_fruitMediumExchange.getAmountPerServing()));
            L_fruitMedium.setUnit(L_fruitMediumExchange.getUnit());

            lunchList.add(L_fruitMedium);

            medium_cho_fruits.remove(L_fruitMediumExchange);
        }


        //Lunch fruitsLowCHO

        if (lunchServings.getFruitsLowCHO() != 0.0) {
            FoodExchange L_fruitLowExchange = low_cho_fruits.get(rand.nextInt(low_cho_fruits.size()));

            Food L_fruitLow = new Food();
            L_fruitLow.setFood(L_fruitLowExchange.getFood());
            L_fruitLow.setFoodGroup(L_fruitLowExchange.getFoodGroup());
            L_fruitLow.setServingsAmount((lunchServings.getFruitsLowCHO()) * (L_fruitLowExchange.getAmountPerServing()));
            L_fruitLow.setUnit(L_fruitLowExchange.getUnit());

            lunchList.add(L_fruitLow);

            low_cho_fruits.remove(L_fruitLowExchange);
        }

        //Lunch vegetablesHighCHO

        if (lunchServings.getVegetablesHighCHO() != 0.0) {
            FoodExchange L_vegetableHighExchange = high_cho_vegetables.get(rand.nextInt(high_cho_vegetables.size()));

            Food L_vegetableHigh = new Food();
            L_vegetableHigh.setFood(L_vegetableHighExchange.getFood());
            L_vegetableHigh.setFoodGroup(L_vegetableHighExchange.getFoodGroup());
            L_vegetableHigh.setServingsAmount((lunchServings.getVegetablesHighCHO()) * (L_vegetableHighExchange.getAmountPerServing()));
            L_vegetableHigh.setUnit(L_vegetableHighExchange.getUnit());

            lunchList.add(L_vegetableHigh);

            high_cho_vegetables.remove(L_vegetableHighExchange);
        }


        //Lunch vegetablesMediumCHO

        if (lunchServings.getVegetablesMediumCHO() != 0.0) {
            FoodExchange L_vegetableMediumExchange = medium_cho_vegetables.get(rand.nextInt(medium_cho_vegetables.size()));

            Food L_vegetableMedium = new Food();
            L_vegetableMedium.setFood(L_vegetableMediumExchange.getFood());
            L_vegetableMedium.setFoodGroup(L_vegetableMediumExchange.getFoodGroup());
            L_vegetableMedium.setServingsAmount((lunchServings.getVegetablesMediumCHO()) * (L_vegetableMediumExchange.getAmountPerServing()));
            L_vegetableMedium.setUnit(L_vegetableMediumExchange.getUnit());

            lunchList.add(L_vegetableMedium);

            medium_cho_vegetables.remove(L_vegetableMediumExchange);
        }

        //Lunch vegetablesLowCHO
        if (lunchServings.getVegetablesLowCHO() != 0.0) {
            FoodExchange L_vegetableLowExchange = low_cho_vegetables.get(rand.nextInt(low_cho_vegetables.size()));

            Food L_vegetableLow = new Food();
            L_vegetableLow.setFood(L_vegetableLowExchange.getFood());
            L_vegetableLow.setFoodGroup(L_vegetableLowExchange.getFoodGroup());
            L_vegetableLow.setServingsAmount((lunchServings.getVegetablesLowCHO()) * (L_vegetableLowExchange.getAmountPerServing()));
            L_vegetableLow.setUnit(L_vegetableLowExchange.getUnit());

            lunchList.add(L_vegetableLow);

            low_cho_vegetables.remove(L_vegetableLowExchange);
        }
        //Lunch vegetablesGreenLeafy

        if (lunchServings.getVegetablesGreenLeafy() != 0.0) {
            FoodExchange L_vegetableGrenLeafyExchange = greeny_leafy_vegetables.get(rand.nextInt(greeny_leafy_vegetables.size()));

            Food L_vegetableGreen = new Food();
            L_vegetableGreen.setFood(L_vegetableGrenLeafyExchange.getFood());
            L_vegetableGreen.setFoodGroup(L_vegetableGrenLeafyExchange.getFoodGroup());
            L_vegetableGreen.setServingsAmount((lunchServings.getVegetablesGreenLeafy()) * (L_vegetableGrenLeafyExchange.getAmountPerServing()));
            L_vegetableGreen.setUnit(L_vegetableGrenLeafyExchange.getUnit());

            lunchList.add(L_vegetableGreen);

            greeny_leafy_vegetables.remove(L_vegetableGrenLeafyExchange);
        }


        //Lunch meat

        if (lunchServings.getMeatHighFat() != 0.0) {
            FoodExchange L_meatExchange = meat.get(rand.nextInt(meat.size()));

            Food L_meat = new Food();
            L_meat.setFood(L_meatExchange.getFood());
            L_meat.setFoodGroup(L_meatExchange.getFoodGroup());
            L_meat.setServingsAmount((lunchServings.getMeatHighFat()) * (L_meatExchange.getAmountPerServing()));
            L_meat.setUnit(L_meatExchange.getUnit());

            lunchList.add(L_meat);

            meat.remove(L_meatExchange);
        }


        //Generating Snack 2 Menu

        List<Food> snack2List = new ArrayList<>();

        //Snack 2 Cereal

        if (snack2Servings.getCereals() != 0.0) {
            FoodExchange S2_cerealExchange = snacks.get(rand.nextInt(snacks.size()));

            Food S2_cereal = new Food();
            S2_cereal.setFood(S2_cerealExchange.getFood());
            S2_cereal.setFoodGroup(S2_cerealExchange.getFoodGroup());
            S2_cereal.setServingsAmount((snack2Servings.getCereals()) * (S2_cerealExchange.getAmountPerServing()));
            S2_cereal.setUnit(S2_cerealExchange.getUnit());

            snack2List.add(S2_cereal);
        }


        //Snack 2 Pulse

        if (snack2Servings.getPulses() != 0.0) {
            FoodExchange S2_pulseExchange = pulses.get(rand.nextInt(pulses.size()));

            Food S2_pulse = new Food();
            S2_pulse.setFood(S2_pulseExchange.getFood());
            S2_pulse.setFoodGroup(S2_pulseExchange.getFoodGroup());
            S2_pulse.setServingsAmount((snack2Servings.getPulses()) * (S2_pulseExchange.getAmountPerServing()));
            S2_pulse.setUnit(S2_pulseExchange.getUnit());

            snack2List.add(S2_pulse);
        }


        //Snack 2 fruitsHighCHO

        if (snack2Servings.getFruitsHighCHO() != 0.0) {
            FoodExchange S2_fruitHighExchange = high_cho_fruits.get(rand.nextInt(high_cho_fruits.size()));

            Food S2_fruitHigh = new Food();
            S2_fruitHigh.setFood(S2_fruitHighExchange.getFood());
            S2_fruitHigh.setFoodGroup(S2_fruitHighExchange.getFoodGroup());
            S2_fruitHigh.setServingsAmount((snack2Servings.getFruitsHighCHO()) * (S2_fruitHighExchange.getAmountPerServing()));
            S2_fruitHigh.setUnit(S2_fruitHighExchange.getUnit());

            snack2List.add(S2_fruitHigh);
        }


        //Snack 2 fruitsMediumCHO

        if (snack2Servings.getFruitsMediumCHO() != 0.0) {
            FoodExchange S2_fruitMediumExchange = medium_cho_fruits.get(rand.nextInt(medium_cho_fruits.size()));

            Food S2_fruitMedium = new Food();
            S2_fruitMedium.setFood(S2_fruitMediumExchange.getFood());
            S2_fruitMedium.setFoodGroup(S2_fruitMediumExchange.getFoodGroup());
            S2_fruitMedium.setServingsAmount((snack2Servings.getFruitsMediumCHO()) * (S2_fruitMediumExchange.getAmountPerServing()));
            S2_fruitMedium.setUnit(S2_fruitMediumExchange.getUnit());

            snack2List.add(S2_fruitMedium);
        }


        //Snack 2 fruitsLowCHO

        if (snack2Servings.getFruitsLowCHO() != 0.0) {
            FoodExchange S2_fruitLowExchange = low_cho_fruits.get(rand.nextInt(low_cho_fruits.size()));

            Food S2_fruitLow = new Food();
            S2_fruitLow.setFood(S2_fruitLowExchange.getFood());
            S2_fruitLow.setFoodGroup(S2_fruitLowExchange.getFoodGroup());
            S2_fruitLow.setServingsAmount((snack2Servings.getFruitsLowCHO()) * (S2_fruitLowExchange.getAmountPerServing()));
            S2_fruitLow.setUnit(S2_fruitLowExchange.getUnit());

            snack2List.add(S2_fruitLow);
        }

        //Snack 2 vegetablesHighCHO

        if (snack2Servings.getVegetablesHighCHO() != 0.0) {
            FoodExchange S2_vegetableHighExchange = high_cho_vegetables.get(rand.nextInt(high_cho_vegetables.size()));

            Food S2_vegetableHigh = new Food();
            S2_vegetableHigh.setFood(S2_vegetableHighExchange.getFood());
            S2_vegetableHigh.setFoodGroup(S2_vegetableHighExchange.getFoodGroup());
            S2_vegetableHigh.setServingsAmount((snack2Servings.getVegetablesHighCHO()) * (S2_vegetableHighExchange.getAmountPerServing()));
            S2_vegetableHigh.setUnit(S2_vegetableHighExchange.getUnit());

            snack2List.add(S2_vegetableHigh);
        }


        //Snack 2 vegetablesMediumCHO

        if (snack2Servings.getVegetablesMediumCHO() != 0.0) {
            FoodExchange S2_vegetableMediumExchange = medium_cho_vegetables.get(rand.nextInt(medium_cho_vegetables.size()));

            Food S2_vegetableMedium = new Food();
            S2_vegetableMedium.setFood(S2_vegetableMediumExchange.getFood());
            S2_vegetableMedium.setFoodGroup(S2_vegetableMediumExchange.getFoodGroup());
            S2_vegetableMedium.setServingsAmount((snack2Servings.getVegetablesMediumCHO()) * (S2_vegetableMediumExchange.getAmountPerServing()));
            S2_vegetableMedium.setUnit(S2_vegetableMediumExchange.getUnit());

            snack2List.add(S2_vegetableMedium);
        }

        //Snack 2 vegetablesLowCHO
        if (snack2Servings.getVegetablesLowCHO() != 0.0) {
            FoodExchange S2_vegetableLowExchange = low_cho_vegetables.get(rand.nextInt(low_cho_vegetables.size()));

            Food S2_vegetableLow = new Food();
            S2_vegetableLow.setFood(S2_vegetableLowExchange.getFood());
            S2_vegetableLow.setFoodGroup(S2_vegetableLowExchange.getFoodGroup());
            S2_vegetableLow.setServingsAmount((snack2Servings.getVegetablesLowCHO()) * (S2_vegetableLowExchange.getAmountPerServing()));
            S2_vegetableLow.setUnit(S2_vegetableLowExchange.getUnit());

            snack2List.add(S2_vegetableLow);
        }
        //Snack 2 vegetablesGreenLeafy

        if (snack2Servings.getVegetablesGreenLeafy() != 0.0) {
            FoodExchange S2_vegetableGrenLeafyExchange = greeny_leafy_vegetables.get(rand.nextInt(greeny_leafy_vegetables.size()));

            Food S2_vegetableGreen = new Food();
            S2_vegetableGreen.setFood(S2_vegetableGrenLeafyExchange.getFood());
            S2_vegetableGreen.setFoodGroup(S2_vegetableGrenLeafyExchange.getFoodGroup());
            S2_vegetableGreen.setServingsAmount((snack2Servings.getVegetablesGreenLeafy()) * (S2_vegetableGrenLeafyExchange.getAmountPerServing()));
            S2_vegetableGreen.setUnit(S2_vegetableGrenLeafyExchange.getUnit());

            snack2List.add(S2_vegetableGreen);
        }

        //Snack 2 milk

        if (snack2Servings.getMilkFresh() != 0.0) {
            FoodExchange S2_milkExchange = milk.get(rand.nextInt(milk.size()));

            Food S2_milk = new Food();
            S2_milk.setFood(S2_milkExchange.getFood());
            S2_milk.setFoodGroup(S2_milkExchange.getFoodGroup());
            S2_milk.setServingsAmount((snack2Servings.getMilkFresh()) * (S2_milkExchange.getAmountPerServing()));
            S2_milk.setUnit(S2_milkExchange.getUnit());

            snack2List.add(S2_milk);
        }

        //Snack 2 meat

        if (snack2Servings.getMeatHighFat() != 0.0) {
            FoodExchange S2_meatExchange = meat.get(rand.nextInt(meat.size()));

            Food S2_meat = new Food();
            S2_meat.setFood(S2_meatExchange.getFood());
            S2_meat.setFoodGroup(S2_meatExchange.getFoodGroup());
            S2_meat.setServingsAmount((snack2Servings.getMeatHighFat()) * (S2_meatExchange.getAmountPerServing()));
            S2_meat.setUnit(S2_meatExchange.getUnit());

            snack2List.add(S2_meat);
        }


        //Generating Dinner Menu

        List<Food> dinnerList = new ArrayList<>();

        //Dinner Cereal

        if (dinnerServings.getCereals() != 0.0) {

            FoodExchange D_cerealExchange = null;

            if (dinnerPrediction.contains("Rice")) {
                D_cerealExchange = rice.get(rand.nextInt(rice.size()));
            }
            if (dinnerPrediction.contains("Bread")) {
                D_cerealExchange = bread.get(rand.nextInt(bread.size()));
            }
            if (dinnerPrediction.contains("Rice Flour Foods")) {
                D_cerealExchange = rice_flour_foods.get(rand.nextInt(rice_flour_foods.size()));
            }
            if (dinnerPrediction.contains("Wheat Flour Foods")) {
                D_cerealExchange = wheat_flour_foods.get(rand.nextInt(wheat_flour_foods.size()));
            }


//            FoodExchange D_cerealExchange = cereals.get(rand.nextInt(cereals.size()));

            Food D_cereal = new Food();
            D_cereal.setFood(D_cerealExchange.getFood());
            D_cereal.setFoodGroup(D_cerealExchange.getFoodGroup());
            D_cereal.setServingsAmount((dinnerServings.getCereals()) * (D_cerealExchange.getAmountPerServing()));
            D_cereal.setUnit(D_cerealExchange.getUnit());

            dinnerList.add(D_cereal);
        }


        //Dinner Pulse

        if (dinnerServings.getPulses() != 0.0) {
            FoodExchange D_pulseExchange = pulses.get(rand.nextInt(pulses.size()));

            Food D_pulse = new Food();
            D_pulse.setFood(D_pulseExchange.getFood());
            D_pulse.setFoodGroup(D_pulseExchange.getFoodGroup());
            D_pulse.setServingsAmount((dinnerServings.getPulses()) * (D_pulseExchange.getAmountPerServing()));
            D_pulse.setUnit(D_pulseExchange.getUnit());

            dinnerList.add(D_pulse);
        }


        //Dinner fruitsHighCHO

        if (dinnerServings.getFruitsHighCHO() != 0.0) {
            FoodExchange D_fruitHighExchange = high_cho_fruits.get(rand.nextInt(high_cho_fruits.size()));

            Food D_fruitHigh = new Food();
            D_fruitHigh.setFood(D_fruitHighExchange.getFood());
            D_fruitHigh.setFoodGroup(D_fruitHighExchange.getFoodGroup());
            D_fruitHigh.setServingsAmount((dinnerServings.getFruitsHighCHO()) * (D_fruitHighExchange.getAmountPerServing()));
            D_fruitHigh.setUnit(D_fruitHighExchange.getUnit());

            dinnerList.add(D_fruitHigh);
        }


        //Dinner fruitsMediumCHO

        if (dinnerServings.getFruitsMediumCHO() != 0.0) {
            FoodExchange D_fruitMediumExchange = medium_cho_fruits.get(rand.nextInt(medium_cho_fruits.size()));

            Food D_fruitMedium = new Food();
            D_fruitMedium.setFood(D_fruitMediumExchange.getFood());
            D_fruitMedium.setFoodGroup(D_fruitMediumExchange.getFoodGroup());
            D_fruitMedium.setServingsAmount((dinnerServings.getFruitsMediumCHO()) * (D_fruitMediumExchange.getAmountPerServing()));
            D_fruitMedium.setUnit(D_fruitMediumExchange.getUnit());

            dinnerList.add(D_fruitMedium);
        }


        //Dinner fruitsLowCHO

        if (dinnerServings.getFruitsLowCHO() != 0.0) {
            FoodExchange D_fruitLowExchange = low_cho_fruits.get(rand.nextInt(low_cho_fruits.size()));

            Food D_fruitLow = new Food();
            D_fruitLow.setFood(D_fruitLowExchange.getFood());
            D_fruitLow.setFoodGroup(D_fruitLowExchange.getFoodGroup());
            D_fruitLow.setServingsAmount((dinnerServings.getFruitsLowCHO()) * (D_fruitLowExchange.getAmountPerServing()));
            D_fruitLow.setUnit(D_fruitLowExchange.getUnit());

            dinnerList.add(D_fruitLow);
        }

        //Dinner vegetablesHighCHO

        if (dinnerServings.getVegetablesHighCHO() != 0.0) {
            FoodExchange D_vegetableHighExchange = high_cho_vegetables.get(rand.nextInt(high_cho_vegetables.size()));

            Food D_vegetableHigh = new Food();
            D_vegetableHigh.setFood(D_vegetableHighExchange.getFood());
            D_vegetableHigh.setFoodGroup(D_vegetableHighExchange.getFoodGroup());
            D_vegetableHigh.setServingsAmount((dinnerServings.getVegetablesHighCHO()) * (D_vegetableHighExchange.getAmountPerServing()));
            D_vegetableHigh.setUnit(D_vegetableHighExchange.getUnit());

            dinnerList.add(D_vegetableHigh);
        }


        //Dinner vegetablesMediumCHO

        if (dinnerServings.getVegetablesMediumCHO() != 0.0) {
            FoodExchange D_vegetableMediumExchange = medium_cho_vegetables.get(rand.nextInt(medium_cho_vegetables.size()));

            Food D_vegetableMedium = new Food();
            D_vegetableMedium.setFood(D_vegetableMediumExchange.getFood());
            D_vegetableMedium.setFoodGroup(D_vegetableMediumExchange.getFoodGroup());
            D_vegetableMedium.setServingsAmount((dinnerServings.getVegetablesMediumCHO()) * (D_vegetableMediumExchange.getAmountPerServing()));
            D_vegetableMedium.setUnit(D_vegetableMediumExchange.getUnit());

            dinnerList.add(D_vegetableMedium);
        }

        //Dinner vegetablesLowCHO
        if (dinnerServings.getVegetablesLowCHO() != 0.0) {
            FoodExchange D_vegetableLowExchange = low_cho_vegetables.get(rand.nextInt(low_cho_vegetables.size()));

            Food D_vegetableLow = new Food();
            D_vegetableLow.setFood(D_vegetableLowExchange.getFood());
            D_vegetableLow.setFoodGroup(D_vegetableLowExchange.getFoodGroup());
            D_vegetableLow.setServingsAmount((dinnerServings.getVegetablesLowCHO()) * (D_vegetableLowExchange.getAmountPerServing()));
            D_vegetableLow.setUnit(D_vegetableLowExchange.getUnit());

            dinnerList.add(D_vegetableLow);
        }
        //Dinner vegetablesGreenLeafy

        if (dinnerServings.getVegetablesGreenLeafy() != 0.0) {
            FoodExchange D_vegetableGrenLeafyExchange = greeny_leafy_vegetables.get(rand.nextInt(greeny_leafy_vegetables.size()));

            Food D_vegetableGreen = new Food();
            D_vegetableGreen.setFood(D_vegetableGrenLeafyExchange.getFood());
            D_vegetableGreen.setFoodGroup(D_vegetableGrenLeafyExchange.getFoodGroup());
            D_vegetableGreen.setServingsAmount((dinnerServings.getVegetablesGreenLeafy()) * (D_vegetableGrenLeafyExchange.getAmountPerServing()));
            D_vegetableGreen.setUnit(D_vegetableGrenLeafyExchange.getUnit());

            dinnerList.add(D_vegetableGreen);
        }


        //Dinner meat

        if (dinnerServings.getMeatHighFat() != 0.0) {
            FoodExchange D_meatExchange = meat.get(rand.nextInt(meat.size()));

            Food D_meat = new Food();
            D_meat.setFood(D_meatExchange.getFood());
            D_meat.setFoodGroup(D_meatExchange.getFoodGroup());
            D_meat.setServingsAmount((dinnerServings.getMeatHighFat()) * (D_meatExchange.getAmountPerServing()));
            D_meat.setUnit(D_meatExchange.getUnit());

            dinnerList.add(D_meat);
        }


        breakfast.setBreakfastFoods(breakfastList);
        snack1.setSnack1Foods(snack1List);
        lunch.setLunchFoods(lunchList);
        snack2.setSnack2Foods(snack2List);
        dinner.setDinnerFoods(dinnerList);

        meal.setBreakfastFoods(breakfast);
        meal.setSnack1Foods(snack1);
        meal.setLunchFoods(lunch);
        meal.setSnack2Foods(snack2);
        meal.setDinnerFoods(dinner);

//        meal.setUserId(mealServings.getUser().getUserId());
//        User user = userDAO.getUserByUserId(mealServings.getUserId());
        meal.setUser(user);


//        mealDAO.save(meal);

        MealDTO mealDTO = new MealDTO();
        mealDTO.setBreakfastFoods(meal.getBreakfastFoods());
        mealDTO.setSnack1Foods(meal.getSnack1Foods());
        mealDTO.setLunchFoods(meal.getLunchFoods());
        mealDTO.setSnack2Foods(meal.getSnack2Foods());
        mealDTO.setDinnerFoods(meal.getDinnerFoods());
        mealDTO.setMealId(meal.getMealId());
        mealDTO.setUserId(meal.getUser().getUserId());
        mealDTO.setRequiredCHO(mealServings.getRequiredCHO());
        mealDTO.setRequiredFat(mealServings.getRequiredFat());
        mealDTO.setRequiredProtein(mealServings.getRequiredProtein());
        mealDTO.setGeneratedCHO(mealServings.getGeneratedCHO());
        mealDTO.setGeneratedProtein(mealServings.getGeneratedProtein());
        mealDTO.setGeneratedFat(mealServings.getGeneratedFat());

        return mealDTO;

//        return meal;


    }


}
