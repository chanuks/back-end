package com.apiit.fyp.backend.Service;

import com.apiit.fyp.backend.DTO.ServingCalculationDTO;
import com.apiit.fyp.backend.DTO.UserDTO;
import com.apiit.fyp.backend.DTO.UserInfoDTO;
import com.apiit.fyp.backend.Entity.NutritionalScreening;
import com.apiit.fyp.backend.Entity.ServingCalculation;
import com.apiit.fyp.backend.Entity.User;
import com.apiit.fyp.backend.Entity.UserInfo;
import com.apiit.fyp.backend.Repository.ServingCalculationDAO;
import com.apiit.fyp.backend.Repository.UserDAO;
import com.apiit.fyp.backend.Repository.UserInfoDAO;
import org.omg.PortableServer.SERVANT_RETENTION_POLICY_ID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ServingCalculationService {

    @Autowired UserInfoDAO userInfoDAO;

    @Autowired ServingCalculationDAO servingCalculationDAO;

    @Autowired
    UserDAO userDAO;




    public UserInfo saveUserInfo(UserInfo userInfo){
        UserInfo newUserInfo = new UserInfo();
        newUserInfo.setBreakfastFood(userInfo.getBreakfastFood());
        newUserInfo.setLunchFood(userInfo.getLunchFood());
        newUserInfo.setDinnerFood(userInfo.getDinnerFood());
        newUserInfo.setEatingPattern(userInfo.getEatingPattern());
        newUserInfo.setUserAllergies(userInfo.getUserAllergies());
        newUserInfo.setUserMedicalConditions(userInfo.getUserMedicalConditions());
        newUserInfo.setDER(userInfo.getDER());
        newUserInfo.setUser(userInfo.getUser());

        return userInfoDAO.save(newUserInfo);




    }

    public ServingCalculation saveServingCalculation(ServingCalculation servingCalculation){

        ServingCalculation newServingCalculation = new ServingCalculation();
        newServingCalculation.setCereals(servingCalculation.getCereals());
        newServingCalculation.setPulses(servingCalculation.getPulses());
        newServingCalculation.setFruitsHighCHO(servingCalculation.getFruitsHighCHO());
        newServingCalculation.setFruitsMediumCHO(servingCalculation.getFruitsMediumCHO());
        newServingCalculation.setFruitsLowCHO(servingCalculation.getFruitsLowCHO());
        newServingCalculation.setVegetablesHighCHO(servingCalculation.getVegetablesHighCHO());
        newServingCalculation.setVegetablesMediumCHO(servingCalculation.getVegetablesMediumCHO());
        newServingCalculation.setVegetablesLowCHO(servingCalculation.getVegetablesLowCHO());
        newServingCalculation.setVegetablesGreenLeafy(servingCalculation.getVegetablesGreenLeafy());
        newServingCalculation.setMilk(servingCalculation.getMilk());
        newServingCalculation.setMeat(servingCalculation.getMeat());
        newServingCalculation.setFatsOils(servingCalculation.getFatsOils());
        newServingCalculation.setNuts(servingCalculation.getNuts());
        newServingCalculation.setCoconut(servingCalculation.getCoconut());
        newServingCalculation.setChoInG(servingCalculation.getChoInG());
        newServingCalculation.setProteinInG(servingCalculation.getProteinInG());
        newServingCalculation.setFatInG(servingCalculation.getFatInG());
        newServingCalculation.setUser(servingCalculation.getUser());

        return servingCalculationDAO.save(servingCalculation);

    }

    public ServingCalculationDTO calculateDailyServings(UserInfoDTO userInfoDTO){

//        User user = userDAO.getUserByUserId(userInfoDTO.getUserId());

        ServingCalculationDTO servingCalculationResponse = new ServingCalculationDTO();
//
        if(userInfoDTO.getEatingPattern().contains("I eat Anything")){
            servingCalculationResponse =  healthyAdultCalculation(userInfoDTO);

        }else if (userInfoDTO.getEatingPattern().contains("Vegan")){
            servingCalculationResponse = vegetarianHealthyAdultCalculation(userInfoDTO);

        } else if (userInfoDTO.getEatingPattern().contains("Lacto-ovo-vegetarian")){
            servingCalculationResponse =lactoOvoDietServing(userInfoDTO);

        }else if (userInfoDTO.getEatingPattern().contains("Pesco-vegetarian")){
            servingCalculationResponse =pescotarianDietServing(userInfoDTO);
        }

        return servingCalculationResponse;

//         double DER = nutritionalScreening.getDER();
//
//         double finalCHOinG = (DER*0.55)/4;
//         double finalProteininG = (DER*0.15)/4;
//         double finalFatinG = (DER*0.30)/9;
//
//         double CHO = 0.0;
//         double Protein = 0.0;
//         double Fat = 0.0;
//
//
//         ServingCalculation servingCalculation = new ServingCalculation();
//
//         servingCalculation.setUser(nutritionalScreening.getUser());
//
//         servingCalculation.setMilk(1);
//         CHO += 12;
//         Protein += 8;
//         Fat += 8;
//
//         servingCalculation.setFruitsHighCHO(1);
//         CHO += 20;
//
//         servingCalculation.setFruitsMediumCHO(1);
//         CHO += 15;
//
//         servingCalculation.setFruitsLowCHO(2);
//         CHO += 20;
//
//         servingCalculation.setVegetablesHighCHO(1);
//         CHO += 15;
//
//         servingCalculation.setFruitsMediumCHO(1);
//         CHO += 10;
//
//         servingCalculation.setVegetablesLowCHO(2);
//         CHO += 10;
//
//         servingCalculation.setVegetablesGreenLeafy(1);
//
//         servingCalculation.setPulses(2);
//         CHO += 24;
//         Protein += 12;
//         Fat += 1;
//
////         Calculating Cereal Servings
//         int cerealServings = (int) ((finalCHOinG-CHO)/15);
//         int cerealCHOInG = cerealServings*15;
//         int cerealProteinInG = cerealServings*2;
//
//         CHO += cerealCHOInG;
//         Protein += cerealProteinInG;
//
////         if (CHO != finalCHOinG){
////             int excessCHO = (int) (CHO - finalCHOinG);
////
////         }
//
//         servingCalculation.setCereals(cerealServings);
//
////         Calculating Meat Servings
//
//        int meatServings = (int) ((finalProteininG-Protein)/7);
//
//        int meatProteinInG = meatServings*7;
//        int meatFat = meatServings*1;
//
//        Protein += meatProteinInG;
//        Fat += meatFat;
//
//        servingCalculation.setMeat(meatServings);
//
//
//        //Calculating Coconut Servings
//
//        servingCalculation.setCoconut(1);
//
//        Fat += 20;
//
//
//        // Fats and oils calculation
//
//        int fatOilServings = (int) ((finalFatinG-Fat)/5);
//
//        int fatoilInG = fatOilServings*5;
//
//        Fat += fatoilInG;
//
//        servingCalculation.setFatsOils(fatOilServings);
//
//        servingCalculation.setChoInG((int) CHO);
//
//        servingCalculation.setProteinInG((int) Protein);
//
//        servingCalculation.setFatInG((int) Fat);
//
//        System.out.println("Required CHO "+finalCHOinG);
//        System.out.println("Required Protein "+finalProteininG);
//        System.out.println("Required Fat "+finalFatinG);
//
//        System.out.println("Calculated CHO "+CHO);
//        System.out.println("Calculated Protein "+Protein);
//        System.out.println("CALCULATED Fat "+Fat);
//        return servingCalculation;


    }


    public ServingCalculationDTO healthyAdultCalculation(UserInfoDTO userInfoDTO){

        double DER = userInfoDTO.getDer();


        double finalCHOinG = (DER*0.55)/4;
        double finalProteininG = (DER*0.15)/4;
        double finalFatinG = (DER*0.30)/9;

        double CHO = 0.0;
        double Protein = 0.0;
        double Fat = 0.0;


        ServingCalculationDTO servingCalculation = new ServingCalculationDTO();

        servingCalculation.setUserId(userInfoDTO.getUserId());

        servingCalculation.setDietType(userInfoDTO.getEatingPattern());

        servingCalculation.setMilk(1);
        CHO += 12;
        Protein += 8;
        Fat += 8;

        servingCalculation.setFruitsHighCHO(1);
        CHO += 20;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 15;

        servingCalculation.setFruitsLowCHO(2);
        CHO += 20;

        servingCalculation.setVegetablesHighCHO(1);
        CHO += 15;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 10;

        servingCalculation.setVegetablesLowCHO(2);
        CHO += 10;

        servingCalculation.setVegetablesGreenLeafy(1);

        servingCalculation.setPulses(2);
        CHO += 24;
        Protein += 12;
        Fat += 1;

//         Calculating Cereal Servings
        int cerealServings = (int) ((finalCHOinG-CHO)/15);
        int cerealCHOInG = cerealServings*15;
        int cerealProteinInG = cerealServings*2;

        CHO += cerealCHOInG;
        Protein += cerealProteinInG;

//         if (CHO != finalCHOinG){
//             int excessCHO = (int) (CHO - finalCHOinG);
//
//         }


        servingCalculation.setCereals(cerealServings);

        //Egg serving

        servingCalculation.setEggs(1);
        Protein += 7;
        Fat += 3;


//         Calculating Meat Servings

        int meatServings = (int) ((finalProteininG-Protein)/7);

        int meatProteinInG = meatServings*7;
        int meatFat = meatServings*1;

        Protein += meatProteinInG;
        Fat += meatFat;

        servingCalculation.setMeat(meatServings);


        //Calculating Coconut Servings

        servingCalculation.setCoconut(1);

        Fat += 20;


        // Fats and oils calculation

        int fatOilServings = (int) ((finalFatinG-Fat)/5);

        int fatoilInG = fatOilServings*5;

        Fat += fatoilInG;

        servingCalculation.setFatsOils(fatOilServings);

        servingCalculation.setChoInG((int) CHO);

        servingCalculation.setProteinInG((int) Protein);

        servingCalculation.setFatInG((int) Fat);

        System.out.println("NORMAL DIET SERVINGS");
        System.out.println("Required CHO "+finalCHOinG);
        System.out.println("Required Protein "+finalProteininG);
        System.out.println("Required Fat "+finalFatinG);

        System.out.println("Calculated CHO "+CHO);
        System.out.println("Calculated Protein "+Protein);
        System.out.println("CALCULATED Fat "+Fat);
        servingCalculation.setRequiredCHO(finalCHOinG);
        servingCalculation.setRequiredProtein(finalProteininG);
        servingCalculation.setRequiredFat(finalFatinG);
        servingCalculation.setGeneratedCHO(CHO);
        servingCalculation.setGeneratedProtein(Protein);
        servingCalculation.setGeneratedFat(Fat);

        return servingCalculation;

    }

    public ServingCalculationDTO vegetarianHealthyAdultCalculation(UserInfoDTO userInfoDTO){
        double DER = userInfoDTO.getDer();

        double finalCHOinG = (DER*0.55)/4;
        double finalProteininG = (DER*0.15)/4;
        double finalFatinG = (DER*0.30)/9;

        double CHO = 0.0;
        double Protein = 0.0;
        double Fat = 0.0;


        ServingCalculationDTO servingCalculation = new ServingCalculationDTO();

        servingCalculation.setUserId(userInfoDTO.getUserId());

        servingCalculation.setDietType(userInfoDTO.getEatingPattern());
        servingCalculation.setMilk(3);
        CHO += 36;
        Protein += 24;
        Fat += 24;

//        servingCalculation.setFruitsHighCHO(1);
//        CHO += 20;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 15;

        servingCalculation.setFruitsLowCHO(2);
        CHO += 20;

//        servingCalculation.setVegetablesHighCHO(1);
//        CHO += 15;

        servingCalculation.setVegetablesMediumCHO(1);
        CHO += 10;

        servingCalculation.setVegetablesLowCHO(3);
        CHO += 6;

        servingCalculation.setVegetablesGreenLeafy(2);

        servingCalculation.setPulses(4);
        CHO += 48;
        Protein += 24;
        Fat += 4;

//         Calculating Cereal Servings
        int cerealServings = (int) ((finalCHOinG-CHO)/15);
        int cerealCHOInG = cerealServings*15;
        int cerealProteinInG = cerealServings*2;

        CHO += cerealCHOInG;
        Protein += cerealProteinInG;

//         if (CHO != finalCHOinG){
//             int excessCHO = (int) (CHO - finalCHOinG);
//
//         }

        servingCalculation.setCereals(cerealServings);

////         Calculating Meat Servings
//
//        int meatServings = (int) ((finalProteininG-Protein)/7);
//
//        int meatProteinInG = meatServings*7;
//        int meatFat = meatServings*1;
//
//        Protein += meatProteinInG;
//        Fat += meatFat;
//
//        servingCalculation.setMeat(meatServings);

        //         Calculating Weighprotein Servings

        int wheyServings = (int) ((finalProteininG-Protein)/7);

        int wheyProteinInG = wheyServings*5;


        Protein += wheyProteinInG;


        servingCalculation.setWheyProtein(wheyServings);
//        servingCalculation.setMeat(wheyServings);


        //Calculating Coconut Servings

        servingCalculation.setCoconut(1);

        Fat += 20;


        // Fats and oils calculation

        int fatOilServings = (int) ((finalFatinG-Fat)/5);

        int fatoilInG = fatOilServings*5;

        Fat += fatoilInG;

        servingCalculation.setFatsOils(fatOilServings);

        servingCalculation.setChoInG((int) CHO);

        servingCalculation.setProteinInG((int) Protein);

        servingCalculation.setFatInG((int) Fat);

        System.out.println("VEGETARIAN DIET SERVINGS");
        System.out.println("Required CHO "+finalCHOinG);
        System.out.println("Required Protein "+finalProteininG);
        System.out.println("Required Fat "+finalFatinG);

        System.out.println("Calculated CHO "+CHO);
        System.out.println("Calculated Protein "+Protein);
        System.out.println("CALCULATED Fat "+Fat);
        servingCalculation.setRequiredCHO(finalCHOinG);
        servingCalculation.setRequiredProtein(finalProteininG);
        servingCalculation.setRequiredFat(finalFatinG);
        servingCalculation.setGeneratedCHO(CHO);
        servingCalculation.setGeneratedProtein(Protein);
        servingCalculation.setGeneratedFat(Fat);
        return servingCalculation;

    }

    public ServingCalculationDTO lactoOvoDietServing(UserInfoDTO userInfoDTO){
        double DER = userInfoDTO.getDer();

        double finalCHOinG = (DER*0.55)/4;
        double finalProteininG = (DER*0.15)/4;
        double finalFatinG = (DER*0.30)/9;

        double CHO = 0.0;
        double Protein = 0.0;
        double Fat = 0.0;


        ServingCalculationDTO servingCalculation = new ServingCalculationDTO();

        servingCalculation.setUserId(userInfoDTO.getUserId());

        servingCalculation.setDietType(userInfoDTO.getEatingPattern());
        servingCalculation.setMilk(2);
        CHO += 24;
        Protein += 16;
        Fat += 16;

//        servingCalculation.setFruitsHighCHO(1);
//        CHO += 20;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 15;

        servingCalculation.setFruitsLowCHO(2);
        CHO += 20;

//        servingCalculation.setVegetablesHighCHO(1);
//        CHO += 15;

        servingCalculation.setVegetablesMediumCHO(1);
        CHO += 10;

        servingCalculation.setVegetablesLowCHO(2);
        CHO += 4;

        servingCalculation.setVegetablesGreenLeafy(2);

        servingCalculation.setPulses(4);
        CHO += 48;
        Protein += 24;
        Fat += 4;

        servingCalculation.setEggs(2);
        Protein += 14;
        Fat += 6;

//         Calculating Cereal Servings
        int cerealServings = (int) ((finalCHOinG-CHO)/15);
        int cerealCHOInG = cerealServings*15;
        int cerealProteinInG = cerealServings*2;

        CHO += cerealCHOInG;
        Protein += cerealProteinInG;

//         if (CHO != finalCHOinG){
//             int excessCHO = (int) (CHO - finalCHOinG);
//
//         }

        servingCalculation.setCereals(cerealServings);

////         Calculating Meat Servings
//
//        int meatServings = (int) ((finalProteininG-Protein)/7);
//
//        int meatProteinInG = meatServings*7;
//        int meatFat = meatServings*1;
//
//        Protein += meatProteinInG;
//        Fat += meatFat;
//
//        servingCalculation.setMeat(meatServings);

        //Calculating


        //         Calculating Weighprotein Servings

        int wheyServings = (int) ((finalProteininG-Protein)/7);

        int wheyProteinInG = wheyServings*5;


        Protein += wheyProteinInG;


        servingCalculation.setWheyProtein(wheyServings);
//        servingCalculation.setMeat(wheyServings);


        //Calculating Coconut Servings

        servingCalculation.setCoconut(1);

        Fat += 20;


        // Fats and oils calculation

        int fatOilServings = (int) ((finalFatinG-Fat)/5);

        int fatoilInG = fatOilServings*5;

        Fat += fatoilInG;

        servingCalculation.setFatsOils(fatOilServings);

        servingCalculation.setChoInG((int) CHO);

        servingCalculation.setProteinInG((int) Protein);

        servingCalculation.setFatInG((int) Fat);

        System.out.println("LACTO-OVO DIET SERVINGS");
        System.out.println("Required CHO "+finalCHOinG);
        System.out.println("Required Protein "+finalProteininG);
        System.out.println("Required Fat "+finalFatinG);

        System.out.println("Calculated CHO "+CHO);
        System.out.println("Calculated Protein "+Protein);
        System.out.println("CALCULATED Fat "+Fat);
        servingCalculation.setRequiredCHO(finalCHOinG);
        servingCalculation.setRequiredProtein(finalProteininG);
        servingCalculation.setRequiredFat(finalFatinG);
        servingCalculation.setGeneratedCHO(CHO);
        servingCalculation.setGeneratedProtein(Protein);
        servingCalculation.setGeneratedFat(Fat);
        return servingCalculation;

    }

    public ServingCalculationDTO lactoDietServing(UserInfoDTO userInfoDTO){
        double DER = userInfoDTO.getDer();


        double finalCHOinG = (DER*0.55)/4;
        double finalProteininG = (DER*0.15)/4;
        double finalFatinG = (DER*0.30)/9;

        double CHO = 0.0;
        double Protein = 0.0;
        double Fat = 0.0;


        ServingCalculationDTO servingCalculation = new ServingCalculationDTO();

        servingCalculation.setUserId(userInfoDTO.getUserId());


        servingCalculation.setMilk(1);
        CHO += 12;
        Protein += 8;
        Fat += 8;

        servingCalculation.setFruitsHighCHO(1);
        CHO += 20;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 15;

        servingCalculation.setFruitsLowCHO(2);
        CHO += 20;

        servingCalculation.setVegetablesHighCHO(1);
        CHO += 15;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 10;

        servingCalculation.setVegetablesLowCHO(2);
        CHO += 10;

        servingCalculation.setVegetablesGreenLeafy(1);

        servingCalculation.setPulses(2);
        CHO += 24;
        Protein += 12;
        Fat += 1;

//         Calculating Cereal Servings
        int cerealServings = (int) ((finalCHOinG-CHO)/15);
        int cerealCHOInG = cerealServings*15;
        int cerealProteinInG = cerealServings*2;

        CHO += cerealCHOInG;
        Protein += cerealProteinInG;

//         if (CHO != finalCHOinG){
//             int excessCHO = (int) (CHO - finalCHOinG);
//
//         }

        servingCalculation.setCereals(cerealServings);

//         Calculating Meat Servings

        int meatServings = (int) ((finalProteininG-Protein)/7);

        int meatProteinInG = meatServings*7;
        int meatFat = meatServings*1;

        Protein += meatProteinInG;
        Fat += meatFat;

        servingCalculation.setMeat(meatServings);


        //Calculating Coconut Servings

        servingCalculation.setCoconut(1);

        Fat += 20;


        // Fats and oils calculation

        int fatOilServings = (int) ((finalFatinG-Fat)/5);

        int fatoilInG = fatOilServings*5;

        Fat += fatoilInG;

        servingCalculation.setFatsOils(fatOilServings);

        servingCalculation.setChoInG((int) CHO);

        servingCalculation.setProteinInG((int) Protein);

        servingCalculation.setFatInG((int) Fat);

        System.out.println("LACTO DIET SERVINGS");
        System.out.println("Required CHO "+finalCHOinG);
        System.out.println("Required Protein "+finalProteininG);
        System.out.println("Required Fat "+finalFatinG);

        System.out.println("Calculated CHO "+CHO);
        System.out.println("Calculated Protein "+Protein);
        System.out.println("CALCULATED Fat "+Fat);
        servingCalculation.setRequiredCHO(finalCHOinG);
        servingCalculation.setRequiredProtein(finalProteininG);
        servingCalculation.setRequiredFat(finalFatinG);
        servingCalculation.setGeneratedCHO(CHO);
        servingCalculation.setGeneratedProtein(Protein);
        servingCalculation.setGeneratedFat(Fat);
        return servingCalculation;

    }

    public ServingCalculationDTO ovoDietServing(UserInfoDTO userInfoDTO){
        double DER = userInfoDTO.getDer();


        double finalCHOinG = (DER*0.55)/4;
        double finalProteininG = (DER*0.15)/4;
        double finalFatinG = (DER*0.30)/9;

        double CHO = 0.0;
        double Protein = 0.0;
        double Fat = 0.0;


        ServingCalculationDTO servingCalculation = new ServingCalculationDTO();

        servingCalculation.setUserId(userInfoDTO.getUserId());


        servingCalculation.setMilk(1);
        CHO += 12;
        Protein += 8;
        Fat += 8;

        servingCalculation.setFruitsHighCHO(1);
        CHO += 20;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 15;

        servingCalculation.setFruitsLowCHO(2);
        CHO += 20;

        servingCalculation.setVegetablesHighCHO(1);
        CHO += 15;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 10;

        servingCalculation.setVegetablesLowCHO(2);
        CHO += 10;

        servingCalculation.setVegetablesGreenLeafy(1);

        servingCalculation.setPulses(2);
        CHO += 24;
        Protein += 12;
        Fat += 1;

//         Calculating Cereal Servings
        int cerealServings = (int) ((finalCHOinG-CHO)/15);
        int cerealCHOInG = cerealServings*15;
        int cerealProteinInG = cerealServings*2;

        CHO += cerealCHOInG;
        Protein += cerealProteinInG;

//         if (CHO != finalCHOinG){
//             int excessCHO = (int) (CHO - finalCHOinG);
//
//         }

        servingCalculation.setCereals(cerealServings);

//         Calculating Meat Servings

        int meatServings = (int) ((finalProteininG-Protein)/7);

        int meatProteinInG = meatServings*7;
        int meatFat = meatServings*1;

        Protein += meatProteinInG;
        Fat += meatFat;

        servingCalculation.setMeat(meatServings);


        //Calculating Coconut Servings

        servingCalculation.setCoconut(1);

        Fat += 20;


        // Fats and oils calculation

        int fatOilServings = (int) ((finalFatinG-Fat)/5);

        int fatoilInG = fatOilServings*5;

        Fat += fatoilInG;

        servingCalculation.setFatsOils(fatOilServings);

        servingCalculation.setChoInG((int) CHO);

        servingCalculation.setProteinInG((int) Protein);

        servingCalculation.setFatInG((int) Fat);

        System.out.println("NORMAL DIET SERVINGS");
        System.out.println("Required CHO "+finalCHOinG);
        System.out.println("Required Protein "+finalProteininG);
        System.out.println("Required Fat "+finalFatinG);

        System.out.println("Calculated CHO "+CHO);
        System.out.println("Calculated Protein "+Protein);
        System.out.println("CALCULATED Fat "+Fat);
        servingCalculation.setRequiredCHO(finalCHOinG);
        servingCalculation.setRequiredProtein(finalProteininG);
        servingCalculation.setRequiredFat(finalFatinG);
        servingCalculation.setGeneratedCHO(CHO);
        servingCalculation.setGeneratedProtein(Protein);
        servingCalculation.setGeneratedFat(Fat);
        return servingCalculation;

    }

    public ServingCalculationDTO pescotarianDietServing(UserInfoDTO userInfoDTO){
        double DER = userInfoDTO.getDer();


        double finalCHOinG = (DER*0.55)/4;
        double finalProteininG = (DER*0.15)/4;
        double finalFatinG = (DER*0.30)/9;

        double CHO = 0.0;
        double Protein = 0.0;
        double Fat = 0.0;


        ServingCalculationDTO servingCalculation = new ServingCalculationDTO();

        servingCalculation.setUserId(userInfoDTO.getUserId());

        servingCalculation.setDietType(userInfoDTO.getEatingPattern());

        servingCalculation.setMilk(1);
        CHO += 12;
        Protein += 8;
        Fat += 8;

        servingCalculation.setFruitsHighCHO(1);
        CHO += 20;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 15;

        servingCalculation.setFruitsLowCHO(2);
        CHO += 20;

        servingCalculation.setVegetablesHighCHO(1);
        CHO += 15;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 10;

        servingCalculation.setVegetablesLowCHO(2);
        CHO += 10;

        servingCalculation.setVegetablesGreenLeafy(1);

        servingCalculation.setPulses(2);
        CHO += 24;
        Protein += 12;
        Fat += 1;

//         Calculating Cereal Servings
        int cerealServings = (int) ((finalCHOinG-CHO)/15);
        int cerealCHOInG = cerealServings*15;
        int cerealProteinInG = cerealServings*2;

        CHO += cerealCHOInG;
        Protein += cerealProteinInG;

//         if (CHO != finalCHOinG){
//             int excessCHO = (int) (CHO - finalCHOinG);
//
//         }



        servingCalculation.setCereals(cerealServings);

//      Eggs servings
        servingCalculation.setEggs(1);
        Protein += 7;
        Fat += 3;



        //Calculate Fish Servings

        int fishServings = (int) ((finalProteininG-Protein)/7);

        int fishProteinInG = fishServings*7;
        int fishFat = fishServings*1;

        Protein += fishProteinInG;
        Fat += fishFat;

        servingCalculation.setFish(fishServings);


        //Calculating Coconut Servings

        servingCalculation.setCoconut(1);

        Fat += 20;


        // Fats and oils calculation

        int fatOilServings = (int) ((finalFatinG-Fat)/5);

        int fatoilInG = fatOilServings*5;

        Fat += fatoilInG;

        servingCalculation.setFatsOils(fatOilServings);

        servingCalculation.setChoInG((int) CHO);

        servingCalculation.setProteinInG((int) Protein);

        servingCalculation.setFatInG((int) Fat);

        System.out.println("PESCOTARIAN DIET SERVINGS");
        System.out.println("Required CHO "+finalCHOinG);
        System.out.println("Required Protein "+finalProteininG);
        System.out.println("Required Fat "+finalFatinG);

        System.out.println("Calculated CHO "+CHO);
        System.out.println("Calculated Protein "+Protein);
        System.out.println("CALCULATED Fat "+Fat);
        servingCalculation.setRequiredCHO(finalCHOinG);
        servingCalculation.setRequiredProtein(finalProteininG);
        servingCalculation.setRequiredFat(finalFatinG);
        servingCalculation.setGeneratedCHO(CHO);
        servingCalculation.setGeneratedProtein(Protein);
        servingCalculation.setGeneratedFat(Fat);
        return servingCalculation;

    }

    public ServingCalculationDTO semiVegetarianDietServing(UserInfoDTO userInfoDTO){
        double DER = userInfoDTO.getDer();


        double finalCHOinG = (DER*0.55)/4;
        double finalProteininG = (DER*0.15)/4;
        double finalFatinG = (DER*0.30)/9;

        double CHO = 0.0;
        double Protein = 0.0;
        double Fat = 0.0;


        ServingCalculationDTO servingCalculation = new ServingCalculationDTO();

        servingCalculation.setUserId(userInfoDTO.getUserId());


        servingCalculation.setMilk(1);
        CHO += 12;
        Protein += 8;
        Fat += 8;

        servingCalculation.setFruitsHighCHO(1);
        CHO += 20;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 15;

        servingCalculation.setFruitsLowCHO(2);
        CHO += 20;

        servingCalculation.setVegetablesHighCHO(1);
        CHO += 15;

        servingCalculation.setFruitsMediumCHO(1);
        CHO += 10;

        servingCalculation.setVegetablesLowCHO(2);
        CHO += 10;

        servingCalculation.setVegetablesGreenLeafy(1);

        servingCalculation.setPulses(2);
        CHO += 24;
        Protein += 12;
        Fat += 1;

//         Calculating Cereal Servings
        int cerealServings = (int) ((finalCHOinG-CHO)/15);
        int cerealCHOInG = cerealServings*15;
        int cerealProteinInG = cerealServings*2;

        CHO += cerealCHOInG;
        Protein += cerealProteinInG;

//         if (CHO != finalCHOinG){
//             int excessCHO = (int) (CHO - finalCHOinG);
//
//         }

        servingCalculation.setCereals(cerealServings);

//         Calculating Meat Servings

        int meatServings = (int) ((finalProteininG-Protein)/7);

        int meatProteinInG = meatServings*7;
        int meatFat = meatServings*1;

        Protein += meatProteinInG;
        Fat += meatFat;

        servingCalculation.setMeat(meatServings);


        //Calculating Coconut Servings

        servingCalculation.setCoconut(1);

        Fat += 20;


        // Fats and oils calculation

        int fatOilServings = (int) ((finalFatinG-Fat)/5);

        int fatoilInG = fatOilServings*5;

        Fat += fatoilInG;

        servingCalculation.setFatsOils(fatOilServings);

        servingCalculation.setChoInG((int) CHO);

        servingCalculation.setProteinInG((int) Protein);

        servingCalculation.setFatInG((int) Fat);

        System.out.println("SEMI VEGETARIAN SERVINGS");
        System.out.println("Required CHO "+finalCHOinG);
        System.out.println("Required Protein "+finalProteininG);
        System.out.println("Required Fat "+finalFatinG);

        System.out.println("Calculated CHO "+CHO);
        System.out.println("Calculated Protein "+Protein);
        System.out.println("CALCULATED Fat "+Fat);
        servingCalculation.setRequiredCHO(finalCHOinG);
        servingCalculation.setRequiredProtein(finalProteininG);
        servingCalculation.setRequiredFat(finalFatinG);
        servingCalculation.setGeneratedCHO(CHO);
        servingCalculation.setGeneratedProtein(Protein);

        servingCalculation.setGeneratedFat(Fat);
        return servingCalculation;

    }
}
